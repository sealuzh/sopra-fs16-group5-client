package ch.uzh.ifi.seal.soprafs16.coltexpress.models;

import android.os.Parcelable;

import java.util.List;

import auto.parcel.AutoParcel;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Turn;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.gson.AutoGson;

/**
 * Created by a-a-hofmann on 29/04/16.
 */
@AutoGson
@AutoParcel
public abstract class Round implements Parcelable {
    public abstract Long getGameId();

    public abstract Long getNthRound();

    public abstract List<Card> getCardStack();

    public abstract List<Turn> getTurns();

    public abstract Long getCurrentTurnIndex();

    public abstract String getEnd();

    public abstract String getImgType();

    public abstract int getStringDescriptionId();

    public static Round create(Long gameId, Long nthRound, List<Card> cardStack, List<Turn> turns,
                               Long currentTurnIndex, String end, String imgType) {
        return builder().gameId(gameId).nthRound(nthRound).cardStack(cardStack).turns(turns)
                .currentTurnIndex(currentTurnIndex).end(end).imgType(imgType).build();
    }

    public static Round create(Long gameId, Long nthRound, List<Card> cardStack, List<Turn> turns,
                               Long currentTurnIndex, String end, String imgType, int stringDescriptionId) {
        return builder().gameId(gameId).nthRound(nthRound).cardStack(cardStack).turns(turns)
                .currentTurnIndex(currentTurnIndex).end(end).imgType(imgType)
                .stringDescriptionId(stringDescriptionId).build();
    }

    public static Builder builder() {
        return new AutoParcel_Round.Builder();
    }

    public abstract Builder toBuilder();

    @AutoParcel.Builder
    public interface Builder {
        Builder gameId(Long gameId);

        Builder nthRound(Long nthRound);

        Builder cardStack(List<Card> cardStack);

        Builder turns(List<Turn> turns);

        Builder currentTurnIndex(Long currentTurnIndex);

        Builder end(String end);

        Builder imgType(String imgType);

        Builder stringDescriptionId(int stringDescriptionId);

        Round build();
    }
}