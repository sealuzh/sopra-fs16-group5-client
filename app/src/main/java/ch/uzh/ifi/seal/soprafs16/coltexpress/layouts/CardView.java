package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoaderFactory;

/**
 * A view for a single card.
 * Created by a-a-hofmann on 21/04/16.
 */
public class CardView extends ImageView {

    /**
     * Card type. Move, Fire, Loot, etc ...
     */
    private CardType type;

    /**
     * Character for card color.
     */
    private Character character;

    private ColorMatrixColorFilter bwFilter;

    public CardView(Context context) {
        super(context);
        bwFilter = initFilter();
    }

    public CardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        bwFilter = initFilter();
    }

    private ColorMatrixColorFilter initFilter() {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        return new ColorMatrixColorFilter(matrix);
    }

    public Character getCharacter() {
        return character;
    }

    public CardType getType() {
        return type;
    }

    /**
     * Sets this view with a card.
     *
     * @param character Character
     * @param type      Move, Fire, Loot, etc ...
     */
    public void setCard(Character character, CardType type) {
        this.type = type;
        this.character = character;

        //Card image.
        Drawable cardImage = ImageLoaderFactory.createLoader(getContext(), character)
                .getCards().get(type);
        setImageDrawable(cardImage);
    }

    @Override
    public String toString() {
        return "CardView{" +
                "type=" + type +
                ", character=" + character +
                '}';
    }

    @Override
    public void setEnabled(boolean enabled) {
        Drawable cardImage = getDrawable().mutate();
        if (cardImage != null) {
            if (enabled) {
                getDrawable().clearColorFilter();
            } else {
                getDrawable().setColorFilter(bwFilter);
            }
        }
    }
}