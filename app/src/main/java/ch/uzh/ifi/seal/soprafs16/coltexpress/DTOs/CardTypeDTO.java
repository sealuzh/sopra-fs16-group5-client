package ch.uzh.ifi.seal.soprafs16.coltexpress.dtos;

import auto.parcel.AutoParcel;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.gson.AutoGson;

/**
 * Created by a-a-hofmann on 29/04/16.
 */
@AutoGson
@AutoParcel
public abstract class CardTypeDTO {
    public abstract CardType type();

    public static CardTypeDTO create(CardType type) {
        return new AutoParcel_CardTypeDTO(type);
    }
}
