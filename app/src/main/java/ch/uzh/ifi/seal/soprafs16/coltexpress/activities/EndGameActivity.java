package ch.uzh.ifi.seal.soprafs16.coltexpress.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.EndPlayerLayout;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Game;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.GameService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.PlayerService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.ReplaceFont;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.GameUtility;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.MusicPlayer;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.Toaster;

/**
 * Created by francesca on 12.05.16.
 */
public class EndGameActivity extends Activity {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(EndGameActivity.class.getSimpleName());

    private MusicPlayer musicPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_game);
        ReplaceFont.replaceDefaultFont(this, "DEFAULT", "CFOldMilwaukee-Regular.ttf");
        getActionBar().setDisplayHomeAsUpEnabled(true);
        initializeEndPlayerLayout();
        musicPlayer = MusicPlayer.getInstance(this);
    }

    /**
     * Called after {@link #onRestoreInstanceState}, {@link #onRestart}, or
     * {@link #onPause}, for your activity to start interacting with the user.
     * This is a good place to begin animations, open exclusive-access devices
     * (such as the camera), etc.
     * <p/>
     * <p>Keep in mind that onResume is not the best indicator that your activity
     * is visible to the user; a system window such as the keyguard may be in
     * front.  Use {@link #onWindowFocusChanged} to know for certain that your
     * activity is visible to the user (for example, to resume a game).
     * <p/>
     * <p><em>Derived classes must call through to the super class's
     * implementation of this method.  If they do not, an exception will be
     * thrown.</em></p>
     *
     * @see #onRestoreInstanceState
     * @see #onRestart
     * @see #onPostResume
     * @see #onPause
     */
    @Override
    protected void onResume() {
        super.onResume();
        musicPlayer.playOrResume(R.raw.silent_film);
    }

    @Override
    protected void onPause() {
        super.onPause();
        musicPlayer.pause();
    }

    /**
     * Called when the activity has detected the user's press of the back
     * key.  The default implementation simply finishes the current activity,
     * but you can override this to do whatever you want.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        PlayerService.getInstance(getBaseContext()).setPlayer(null);
        GameService.getInstance(this).setGame(null);
        Intent intent = new Intent(this, GameMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void initializeEndPlayerLayout() {
        Game game = GameService.getInstance(this).getGame();
        GameUtility gameUtil = new GameUtility(game, PlayerService.getInstance(this).getPlayer().getId());

        LinearLayout endPlayerContainer = (LinearLayout) findViewById(R.id.finalPLayerList);
        endPlayerContainer.setWeightSum(1.0f);

        Player quitter = gameUtil.getQuitter();

        Player winner = gameUtil.getWinner();
        List<Player> gunslingers = gameUtil.getGunslingers(quitter);
        List<Player> players = game.getPlayers();

        if (quitter != null) {
            new Toaster(getBaseContext())
                    .setText(String.format("Player: %s quit the game!", quitter.getUsername()))
                    .setLayout(R.layout.toast_custom)
                    .makeAToast().show();
        }

        int nrPlayers = players.size();

        // Compute weight
        float weight = 1.0f / nrPlayers;

        for (Player p : players) {
            // Create an overview layout
            EndPlayerLayout e = new EndPlayerLayout(this);

            // Set player info
            boolean isBestShooter = gunslingers.contains(p);

            e.setInfo(p, isBestShooter);

            if (winner.equals(p)) {
                e.setWinner();
            }

            // set weight
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) e.getLayoutParams();
            if (params != null) {
                params.weight = weight;
            } else {
                params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, weight);
            }
            e.setLayoutParams(params);

            endPlayerContainer.addView(e);
        }
    }
}