package ch.uzh.ifi.seal.soprafs16.coltexpress.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;

public class GameCreateFragment extends DialogFragment {

    private EditText text;

    private GameCreateDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.fragment_game_create, null);
        builder.setView(v);
        text = (EditText) v.findViewById(R.id.text);

        builder.setMessage(R.string.game_create_fragment);

        builder.setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (listener != null) {
                    listener.onGameCreateDialog(text.getText().toString());
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dismiss();
            }
        });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public void setOnGameCreateDialogClickListener(GameCreateDialogListener listener) {
        this.listener = listener;
    }

    public interface GameCreateDialogListener {
        void onGameCreateDialog(String gameName);
    }
}
