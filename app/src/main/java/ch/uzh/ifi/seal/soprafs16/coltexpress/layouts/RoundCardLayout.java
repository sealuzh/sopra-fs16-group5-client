package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Turn;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoaderFactory;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Card;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Round;
import it.sephiroth.android.library.tooltip.Tooltip;

/**
 * Round card layout.
 * Created by a-a-hofmann on 19/04/16.
 */
public class RoundCardLayout extends FrameLayout {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(RoundCardLayout.class.getSimpleName());

    private ImageView roundCard;

    private Round currentRound;

    private List<ImageView> turnHighlights;

    private Map<String, Integer> roundDescriptionMap;

    public RoundCardLayout(Context context) {
        super(context);
        initializeViews(context);
    }

    public RoundCardLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public RoundCardLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.round_card_layout, this);

        turnHighlights = new ArrayList<>();

        currentRound = Round.create(1L, 1L, new ArrayList<Card>(), new ArrayList<Turn>(), 0L, "",
                "", R.string.angry_marshal_description);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        LinearLayout turnLayout = (LinearLayout) findViewById(R.id.turnHighlight);

        roundCard = (ImageView) findViewById(R.id.card);

        roundCard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showRoundDescription();
            }
        });

        turnHighlights.add((ImageView) turnLayout.findViewById(R.id.t1));
        turnHighlights.add((ImageView) turnLayout.findViewById(R.id.t2));
        turnHighlights.add((ImageView) turnLayout.findViewById(R.id.t3));
        turnHighlights.add((ImageView) turnLayout.findViewById(R.id.t4));
        turnHighlights.add((ImageView) turnLayout.findViewById(R.id.t5));

        roundDescriptionMap = new HashMap<>();
        roundDescriptionMap.put("ndn", R.string.none_description);
        roundDescriptionMap.put("nhdn", R.string.take_it_all_description);
        roundDescriptionMap.put("nhnhn", R.string.none_description);
        roundDescriptionMap.put("nhnn", R.string.swivel_arm_description);
        roundDescriptionMap.put("nnhnn", R.string.passengers_rebellion_description);
        roundDescriptionMap.put("nnhr", R.string.angry_marshal_description);
        roundDescriptionMap.put("nnnn", R.string.braking_description);
        roundDescriptionMap.put("1", R.string.pickpocketing_description); //Pickpocketing
        roundDescriptionMap.put("2", R.string.marshals_revenge_description); //Marshal
        roundDescriptionMap.put("3", R.string.hostage_taking_conductor_description); //Hostage

        setAllInvisible();
    }

    private void showRoundDescription() {
        Tooltip.TooltipView tooltip = Tooltip.make(getContext(),
                new Tooltip.Builder(101)
                        .anchor(roundCard, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 10000)
                        .activateDelay(800)
                        .showDelay(300)
                        .text(getResources(), roundDescriptionMap.get(currentRound.getImgType()))
                        .maxWidth(500)
                        .withArrow(true)
                        .withOverlay(false)
                        .withStyleId(R.style.ToolTipLayoutCustomStyle)
                        .floatingAnimation(Tooltip.AnimationBuilder.SLOW)
                        .build()
        );
        tooltip.setTextColor(R.color.black);
        tooltip.show();
    }

    private void setAllInvisible() {
        for (ImageView v : turnHighlights) {
            v.setVisibility(INVISIBLE);
        }
    }

    public void setCurrentRound(Round currentRound) {
        this.currentRound = currentRound;
        setRoundCard();
    }

    public void setRoundCard() {
        roundCard.setImageDrawable(ImageLoaderFactory.getRoundCard(getContext(), currentRound.getImgType()));
    }

    public void displayCurrentTurn(Long turn) {
        if ("ndn".equals(currentRound.getImgType())) {
            displayNDN(turn);
        } else if ("nhdn".equals(currentRound.getImgType())) {
            displayNHDN(turn);
        } else {

            switch ((int) turn.longValue()) {

                case 1:
                    turnHighlights.get(0).setVisibility(VISIBLE);
                    disableAllTurnHighlightsAfterTurn(1);
                    break;
                case 2:
                    turnHighlights.get(1).setVisibility(VISIBLE);
                    disableAllTurnHighlightsAfterTurn(2);
                    break;
                case 3:
                    turnHighlights.get(2).setVisibility(VISIBLE);
                    disableAllTurnHighlightsAfterTurn(3);
                    break;
                case 4:
                    turnHighlights.get(3).setVisibility(VISIBLE);
                    disableAllTurnHighlightsAfterTurn(4);
                    break;
                case 5:
                    turnHighlights.get(4).setVisibility(VISIBLE);
                    break;
                default:
                    setAllInvisible();
                    break;
            }
        }
    }

    private void displayNDN(Long turn) {
        switch ((int) turn.longValue()) {

            case 1:
                turnHighlights.get(0).setVisibility(VISIBLE);
                disableAllTurnHighlightsAfterTurn(1);
                break;
            case 2:
                turnHighlights.get(2).setVisibility(VISIBLE);
                disableAllTurnHighlightsAfterTurn(3);
                break;
            case 3:
                turnHighlights.get(3).setVisibility(VISIBLE);
                disableAllTurnHighlightsAfterTurn(4);
                break;
            default:
                setAllInvisible();
                break;
        }
    }

    private void displayNHDN(Long turn) {
        switch ((int) turn.longValue()) {

            case 1:
                turnHighlights.get(0).setVisibility(VISIBLE);
                disableAllTurnHighlightsAfterTurn(1);
                break;
            case 2:
                turnHighlights.get(1).setVisibility(VISIBLE);
                disableAllTurnHighlightsAfterTurn(2);
                break;
            case 3:
                turnHighlights.get(3).setVisibility(VISIBLE);
                disableAllTurnHighlightsAfterTurn(4);
                break;
            case 4:
                turnHighlights.get(4).setVisibility(VISIBLE);
                break;
            default:
                setAllInvisible();
                break;
        }
    }

    private void disableAllTurnHighlightsAfterTurn(int turn) {
        for (int i = turn + 1; i < turnHighlights.size(); i++) {
            turnHighlights.get(i).setVisibility(INVISIBLE);
        }
    }
}