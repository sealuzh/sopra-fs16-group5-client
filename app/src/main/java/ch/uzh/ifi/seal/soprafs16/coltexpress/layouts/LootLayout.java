package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;

/**
 * Created by a-a-hofmann on 19/04/16.
 */
public class LootLayout extends LinearLayout {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(LootLayout.class.getSimpleName());

    private ImageView bag;
    private ImageView diamond;
    private ImageView suitcase;
    private TextView textBag;
    private TextView textDiamond;
    private TextView textSuitcase;
    private int countPurse;
    private int countJewel;
    private int countSuitcase;

    public LootLayout(Context context) {
        super(context);
        initializeViews(context, null);
    }

    public LootLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context, attrs);
    }


    private void initializeViews(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.loot_layout_h, this);

        // get all attributes
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.LootLayout);
        int orientation = ta.getInt(R.styleable.LootLayout_orientation, -1);
        ta.recycle();

        if (orientation == -1) {
            logger.severe("No orientation set: setting horizontal as default");
        } else {
            ((LinearLayout) findViewById(R.id.parent)).setOrientation((orientation == 1) ? HORIZONTAL : VERTICAL);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        // Sets the images for the previous and next buttons. Uses
        // built-in images so you don't need to add images, but in
        // a real application your images should be in the
        // application package so they are always available.
        bag = (ImageView) findViewById(R.id.bag);
        textBag = (TextView) findViewById(R.id.bagText);

        diamond = (ImageView) findViewById(R.id.diamond);
        textDiamond = (TextView) findViewById(R.id.diamondText);

        suitcase = (ImageView) findViewById(R.id.suitcase);
        textSuitcase = (TextView) findViewById(R.id.suitcaseText);

        setLoots(-1, -1, -1);
    }

    public void setLoots(Integer purses, Integer jewels, Integer suitcases) {
        countPurse = purses;
        countJewel = jewels;
        countSuitcase = suitcases;

        textBag.setText(purses.toString());
        textDiamond.setText(jewels.toString());
        textSuitcase.setText(suitcases.toString());

        setVisibility();
    }

    private void setVisibility() {
        if (countPurse == 0) {
            setVisibility(bag, textBag, false);
        } else {
            setVisibility(bag, textBag, true);
        }

        if (countJewel == 0) {
            setVisibility(diamond, textDiamond, false);
        } else {
            setVisibility(diamond, textDiamond, true);
        }

        if (countSuitcase == 0) {
            setVisibility(suitcase, textSuitcase, false);
        } else {
            setVisibility(suitcase, textSuitcase, true);
        }
    }

    private void setVisibility(ImageView image, TextView text, boolean flag) {
        if (!flag) {
            image.setVisibility(INVISIBLE);
            text.setVisibility(INVISIBLE);
        } else {
            image.setVisibility(VISIBLE);
            text.setVisibility(VISIBLE);
        }
    }

    public void emptyLayout() {
        countPurse = 0;
        countJewel = 0;
        countSuitcase = 0;
        setVisibility();
    }
}
