package ch.uzh.ifi.seal.soprafs16.coltexpress.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Types.BoomType;
import com.nightonke.boommenu.Types.ButtonType;
import com.nightonke.boommenu.Types.PlaceType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.GameStatus;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Turn;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PossibilityDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.fragments.GameExitFragment;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.CardView;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.HandLayout;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.PlayerOverviewLayout;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.TopCenterLayout;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoaderFactory;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Card;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Game;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Round;
import ch.uzh.ifi.seal.soprafs16.coltexpress.popups.OnPossibilityChosenListener;
import ch.uzh.ifi.seal.soprafs16.coltexpress.popups.PopupController;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.GameService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.PlayerService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.ReplaceFont;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.RoundService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.GameUtility;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.LootLayoutUtility;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.MeepleLayoutUtility;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.MusicPlayer;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.OpponentsOverviewLayoutUtility;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.Toaster;


@SuppressWarnings("deprecation")
public class GameActivity extends Activity implements GameService.GameReceivedListener,
        RoundService.OnRoundReceivedListener, HandLayout.OnCardClickedListener,
        PlayerOverviewLayout.OnDrawClickListener, OnPossibilityChosenListener,
        RoundService.OnPossibilitiesReceivedListener, RoundService.OnPossibilityPlayedListener, GameUtility.OnTurnChangeListener, GameUtility.OnGameFinishedListener {

    private Logger logger = Logger.getLogger(this.getClass().getSimpleName());

    private Game game;

    private Player player;

    private SoundPool sp;

    private int[] soundIds;

    private GameStatus previousStatus;

    private List<Player> opponents;

    private GameUtility gameUtil;

    private GameService gameService;

    private RoundService roundService;

    private FrameLayout container;

    private HandLayout handLayout;

    private TopCenterLayout topCenterLayout;

    private PlayerOverviewLayout playerOverviewLayout;

    private OpponentsOverviewLayoutUtility opponentUtil;

    private LootLayoutUtility lootUtil;

    private MeepleLayoutUtility meepleUtil;

    private GameRefresher refresher;

    private boolean hasSentNetworkCall;

    private MusicPlayer musicPlayer;

    private Round round;

    private boolean hasAlreadyShownDialog;

    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ReplaceFont.replaceDefaultFont(this, "DEFAULT", "CFOldMilwaukee-Regular.ttf");
        opponents = new ArrayList<>();
        musicPlayer = MusicPlayer.getInstance(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Game loading, please wait.");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Initial state of the game
        player = PlayerService.getInstance(this).getPlayer();
        previousStatus = GameStatus.PENDING;
        gameService = GameService.getInstance(this);
        game = gameService.getGame();

        gameService.setOnGameReceivedListener(this);

        gameUtil = new GameUtility(game, player.getId());
        gameUtil.setOnTurnChangedListener(this);
        gameUtil.setOnGameFinishedListener(this);
        opponents = gameUtil.getOpponents();

        initLayouts();

        roundService = RoundService.getInstance(this);
        roundService.addOnRoundReceivedListener(this);
        roundService.addOnPossibilitiesReceivedListener(this);
        roundService.addOnCardPlayedListener(new RoundService.OnCardPlayedListener() {
            @Override
            public void onCardPlayed() {
                // If draw card succeded then update the game
                gameService.getGameFromNetwork(game.getId());
            }
        });
        roundService.getRound(game.getId());

        initRefresher();

        fetchPossibilities();

        initSoundPool();

        fetchPossibilities();

        musicPlayer.playOrResume(R.raw.good_bad_ugly);
    }

    private void initRefresher() {
        gameService.setOnGameReceivedListener(this);
        refresher = new GameRefresher();
        refresher.execute();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        final int exit = 0;
        final int reconnect = 1;


        // Set string for menu
        String[] sArr = new String[2];
        sArr[exit] = "exit";
        sArr[reconnect] = "reconnect";

        // Set colors for menu. Each tuple is a menu button
        int[][] colorArr = new int[2][2];
        int[] exitColors = {R.color.material_white, R.color.transparent};
        int[] reconnectColors = {R.color.yellow_border, R.color.transparent};

        colorArr[exit] = exitColors;
        colorArr[reconnect] = reconnectColors;

        BoomMenuButton boomMenuButton = (BoomMenuButton) findViewById(R.id.boom);

        new BoomMenuButton.Builder()
                .addSubButton(this, R.drawable.exit2, colorArr[0], sArr[exit])
                .addSubButton(this, R.drawable.refresh, colorArr[0], sArr[reconnect])
                .button(ButtonType.CIRCLE)
                .boom(BoomType.PARABOLA)
                .place(PlaceType.CIRCLE_2_1)
                .init(boomMenuButton);

        boomMenuButton.setOnSubButtonClickListener(new BoomMenuButton.OnSubButtonClickListener() {
            /**
             *
             * @param buttonIndex The index of button that was clicked.
             */
            @Override
            public void onClick(int buttonIndex) {
                if (buttonIndex == exit) {
                    onBackPressed();
                } else if (buttonIndex == reconnect) {
                    // Force refresh
                    cleanup();
                    initRefresher();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        refresher.cancel(true);
        cleanup();

        musicPlayer.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        musicPlayer.close();
    }

    /**
     * Called when the activity has detected the user's press of the back
     * key.  The default implementation simply finishes the current activity,
     * but you can override this to do whatever you want.
     */
    @Override
    public void onBackPressed() {
        final GameExitFragment gameExitFragment = new GameExitFragment();
        gameExitFragment.setOnGameExitConfirmedListener(new GameExitFragment.OnGameExitConfirmedListener() {
            @Override
            public void onGameExitConfirmed() {
                // Send exit game signal
                gameService.exitGame(game.getId(), player.getToken());

                gameService.setGame(null);
                PlayerService.getInstance(getBaseContext()).setPlayer(null);

                Intent intent = new Intent(GameActivity.this, GameMenu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        gameExitFragment.show(getFragmentManager(), "game exit");

    }

    /**
     * Cleanup. Removes listeners.
     */
    private void cleanup() {
        gameService.removeOnGameReceivedListener();
        roundService.removeOnRoundReceivedListener();
        hasSentNetworkCall = false;
    }

    private void initSoundPool() {
        sp = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        soundIds = new int[3];
        soundIds[0] = sp.load(getApplicationContext(), R.raw.train_whistle, 1);
        soundIds[1] = sp.load(getApplicationContext(), R.raw.shoot_sound, 1);
        soundIds[2] = sp.load(getApplicationContext(), R.raw.punch, 1);
    }

    /**
     * Initializes all layouts.
     */
    private void initLayouts() {
        // Set xml file
        int nPlayers = game.getPlayers().size();
        if (nPlayers < 3) {
            setContentView(R.layout.activity_game2);
        } else if (nPlayers == 3) {
            setContentView(R.layout.activity_game3);
        } else if (nPlayers == 4) {
            setContentView(R.layout.activity_game4);
        }

        // Set all layouts
        topCenterLayout = (TopCenterLayout) findViewById(R.id.topCenter);
        initializeHandLayout(player);
        initializePlayerOverviewLayout(player);
        initializeOpponentsOverview();
        container = (FrameLayout) findViewById(R.id.FrameLayout);

        meepleUtil = new MeepleLayoutUtility(this, nPlayers + 1);
        meepleUtil.addOnPossibilityChosenListener(this);
        lootUtil = new LootLayoutUtility(this, nPlayers + 1);
    }

    /**
     * Prepares popups for loot, punch, fire.
     *
     * @param possibilities Possibilities to be displayed.
     */
    private void showTargetPopup(PossibilityDTO possibilities) {
        container.setForeground(getResources().getDrawable(R.drawable.dim));
        container.getForeground().setAlpha(180);

        PopupController popupController = new PopupController(topCenterLayout, container, getBaseContext());
        popupController.addOnPossibilityChosenListener(this);
        popupController.setPossibilities(possibilities);
        popupController.show();
    }

    /**
     * Init hand layout.
     *
     * @param player Player.
     */
    private void initializeHandLayout(Player player) {
        handLayout = (HandLayout) findViewById(R.id.hand);
        handLayout.initCardImages(player);
        handLayout.addOnCardClickedListener(this);
    }

    /**
     * Init player overview layout.
     *
     * @param player Player.
     */
    private void initializePlayerOverviewLayout(Player player) {
        playerOverviewLayout = (PlayerOverviewLayout) findViewById(R.id.player1);
        playerOverviewLayout.setMyOverview(player);
        playerOverviewLayout.addOnDrawClickListener(this);
    }

    /**
     * Initialize opponents overview.
     */
    private void initializeOpponentsOverview() {
        opponentUtil = new OpponentsOverviewLayoutUtility(this, opponents.size());
        opponentUtil.update(opponents, game.getCurrentPlayerId());
    }

    @Override
    public void onGameReceived(Game game) {
        logger.info("onGameReceived(): " + game.toString());
        if (!hasAlreadyShownDialog) {
            progressDialog.dismiss();
            hasAlreadyShownDialog = true;
        }

        updateModels(game);
    }

    /**
     * Update game models.
     *
     * @param game Updated game
     */
    private void updateModels(Game game) {
        gameUtil.update(game);
        this.game = game;
        this.player = gameUtil.getPlayer();
        this.opponents = gameUtil.getOpponents();

        // If the round changed, fetch the newest round config.
        if (gameUtil.hasRoundChanged()) {

            // TOOT TOOOOOOT
            sp.play(soundIds[0], 1, 1, 1, 0, 1.0f);

            // Set backside card as top deck card
            topCenterLayout.setDefaultTopDeckCard();

            gameService.removeOnGameReceivedListener();
            if (refresher != null) {
                refresher.cancel(true);
            }
            final Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    restartRefresher();
                    updateUI();
                }
            }, 3000);
            roundService.getRound(game.getId());

        } else {
            updateUI();
        }

        fetchPossibilities();
    }


    private void restartRefresher() {
        gameService.setOnGameReceivedListener(this);
        refresher = new GameRefresher();
        refresher.execute();
    }

    private void fetchPossibilities() {
        if (gameUtil.isActionPhase() && gameUtil.isMyTurn() && !hasSentNetworkCall) {
            gameService.removeOnGameReceivedListener();

            if (refresher != null) {
                refresher.cancel(true);
            }

            hasSentNetworkCall = true;

            roundService.addOnPossibilitiesReceivedListener(this);
            roundService.getPossibilities(game.getId(), player.getToken());
        }

        // If the game is finished, show the final overview
        if (game.getStatus().equals(GameStatus.FINISHED)) {
            Intent intent = new Intent(GameActivity.this, EndGameActivity.class);
            startActivity(intent);
        }
    }


    /**
     * Updates all UI elements.
     */
    private void updateUI() {
        // Update opponent overview
        opponentUtil.update(opponents, game.getCurrentPlayerId());

        updatePlayerOverview();

        // Draw loots
        lootUtil.drawLoots(game.getLoots());

        // Draw meeples
        meepleUtil.drawMeeples(game);

        updatePlayerHand();

        // Disable draw button when not in current turn
        playerOverviewLayout.setEnabled(gameUtil.isMyTurn(), game.getStatus());

        // Show indication about game phase
        displayPhaseInfo(game.getStatus());

        Card card = game.getCurrentCard();
        if (card != null) {
            // Show last played card
            topCenterLayout.setTopDeckCard(card, gameUtil);
        }

        updateTopCenterOverview();

        if (gameUtil.hasPlayerBeenShot()) {
            showShotEffect();
        } else if (gameUtil.hasPlayerBeenPunched()) {
            showPunchEffect();
        } else {
            logger.severe("I wasn't punched!");
        }
    }

    private void showPunchEffect() {
        logger.severe("I was punched!");
        sp.play(soundIds[2], 1, 1, 1, 0, 1.0f);
        new Toaster(getBaseContext())
                .setLayout(R.layout.toast_punch)
                .setGravity(Gravity.CENTER)
                .setDuration(Toast.LENGTH_SHORT)
                .makeAToast().show();
    }

    private void showShotEffect() {
        sp.play(soundIds[1], 1, 1, 1, 0, 1.0f);
        new Toaster(getBaseContext())
                .setLayout(R.layout.toast_bloody)
                .setGravity(Gravity.CENTER)
                .setDuration(Toast.LENGTH_SHORT)
                .makeAToast().show();
    }

    private void updateTopCenterOverview() {
        topCenterLayout.displayCurrentTurn(game);
        topCenterLayout.setRounds(game.getRoundId(), game.getStatus());
    }

    private void updatePlayerHand() {
        handLayout.updatePlayerHand(player, gameUtil.isMyTurn(), game.getStatus());
        handLayout.setAllowedToPlayACard(gameUtil.isMyTurn());
    }

    private void updatePlayerOverview() {
        playerOverviewLayout.setLoots(player);
        playerOverviewLayout.setBullets(player.getBullets());
        playerOverviewLayout.setHighlighted(gameUtil.isMyTurn());
    }

    /**
     * Displays phase info into a toast.
     *
     * @param status GameStatus to be displayed.
     */
    private void displayPhaseInfo(GameStatus status) {
        if (!previousStatus.equals(status)) {

            String text;

            if (status.equals(GameStatus.PLANNINGPHASE)) {
                text = "Planningphase: play or draw cards";

                new Toaster(getBaseContext())
                        .setText(text)
                        .setLayout(R.layout.toast_custom)
                        .makeAToast().show();
            } else if (status.equals(GameStatus.ACTIONPHASE)) {
                text = "Actionsphase: Perform the action according to the card";

                new Toaster(getBaseContext())
                        .setText(text)
                        .setLayout(R.layout.toast_custom)
                        .makeAToast().show();

            }
            previousStatus = status;

            ImageView image = (ImageView) findViewById(R.id.cactus_place);
            image.setImageResource(R.drawable.cactus);

            TranslateAnimation animation = new TranslateAnimation(1524.0f, -40.0f,
                    0.0f, 0.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
            animation.setDuration(3000);  // animation duration
            animation.setRepeatCount(3);  // animation repeat count

            image.startAnimation(animation);

            image.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onCardClicked(CardView cView) {
        roundService.playCard(game.getId(), game.getRoundId(), cView.getType(), player.getToken());
    }

    @Override
    public void onDrawClicked() {
        // Draw only if it is my turn
        if (gameUtil.isMyTurn()) {
            // Send draw game request to server
            roundService.drawCards(game.getId(), game.getRoundId(), player.getToken());
        }
    }

    @Override
    public void onRoundReceived(Round currentRound) {
        if (round != null) {
            Map<String, Integer> roundDescriptionMap = new HashMap<>();
            roundDescriptionMap.put("ndn", R.string.none_description);
            roundDescriptionMap.put("nhdn", R.string.take_it_all_description);
            roundDescriptionMap.put("nhnhn", R.string.none_description);
            roundDescriptionMap.put("nhnn", R.string.swivel_arm_description);
            roundDescriptionMap.put("nnhnn", R.string.passengers_rebellion_description);
            roundDescriptionMap.put("nnhr", R.string.angry_marshal_description);
            roundDescriptionMap.put("nnnn", R.string.braking_description);
            roundDescriptionMap.put("1", R.string.pickpocketing_description); //Pickpocketing
            roundDescriptionMap.put("2", R.string.marshals_revenge_description); //Marshal
            roundDescriptionMap.put("3", R.string.hostage_taking_conductor_description); //Hostage
            String text = getResources().getString(roundDescriptionMap.get(round.getImgType()));
            new Toaster(getBaseContext())
                    .setText("Round end event: " + text.replaceAll("<br>", " "))
                    .setLayout(R.layout.toast_roudnend)
                    .setGravity(Gravity.CENTER)
                    .setBackgroundDrawable(ImageLoaderFactory.getRoundCard(getBaseContext(), round.getImgType()))
                    .makeAToast().show();

        }

        if (currentRound != null) {
            topCenterLayout.setRound(currentRound);
        }
        round = currentRound;
    }

    @Override
    public void onPossibilitiesReceived(PossibilityDTO possibilities) {
        logger.severe("onPossibilityReceived: " + possibilities.toString());

        topCenterLayout.setTopDeckCardInActionPhase(possibilities.getType(), player.getCharacter());

        boolean hasPossibilities;
        if (CardType.ROBBERY.equals(possibilities.getType())) {
            hasPossibilities = !possibilities.getLoots().isEmpty();
        } else {
            hasPossibilities = CardType.MARSHAL.equals(possibilities.getType()) ||
                    !possibilities.getPlayers().isEmpty();
        }

        if (hasPossibilities) {
            // Send back possibility
            switch (possibilities.getType()) {
                case PUNCH:
                case FIRE:
                case ROBBERY:
                    showTargetPopup(possibilities);
                    break;
                case MOVE:
                case FLOORCHANGE:
                    meepleUtil.drawMoves(possibilities);
                    break;
                case MARSHAL:
                    meepleUtil.drawMarshalMoves(possibilities);
                    break;
                default:
                    onPossibilityChosen(possibilities);
                    break;
            }

            String text = "You played: " + possibilities.getType().toString();
            new Toaster(getBaseContext())
                    .setText(text)
                    .setLayout(R.layout.toast_custom)
                    .setDuration(Toast.LENGTH_SHORT)
                    .makeAToast().show();
        } else {

            String text = "You played: " + possibilities.getType().toString() +
                    " No possible move. Passing.";
            new Toaster(getBaseContext())
                    .setText(text)
                    .setLayout(R.layout.toast_custom)
                    .setDuration(Toast.LENGTH_SHORT)
                    .makeAToast().show();

            // Do nothing and pass the turn
            onPossibilityChosen(possibilities);
        }
    }

    @Override
    public void onPossibilityChosen(PossibilityDTO possibility) {
        logger.severe("Sending possibility to server: " + possibility.toString());

        roundService.playPossibility(game.getId(), player.getToken(), possibility, this);
    }

    @Override
    public void onPossibilityPlayed() {
        gameService.setOnGameReceivedListener(this);
        refresher = new GameRefresher();
        refresher.execute();

        hasSentNetworkCall = false;
    }

    @Override
    public void onTurnChanged(Long turnId) {
        if (round != null && turnId < round.getTurns().size()) {
            Turn currentTurn = round.getTurns().get(turnId.intValue() - 1);
            if (!Turn.NORMAL.equals(currentTurn)) {
                new Toaster(getBaseContext()).setLayout(R.layout.toast_custom)
                        .setText(currentTurn.getTurnString()).makeAToast().show();
            }
        }
    }

    @Override
    public void onGameFinished() {
        game = game.toBuilder().status(GameStatus.FINISHED).build();
        cleanup();
        Intent intent = new Intent(GameActivity.this, EndGameActivity.class);
        startActivity(intent);

        cleanup();
        // Trigger round card toast at the end of the game as well.
        onRoundReceived(null);

        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(GameActivity.this, EndGameActivity.class);
                startActivity(intent);
            }
        }, 3000);
    }

    /**
     * Handles refreshing current game on a separate thread.
     */
    public class GameRefresher extends AsyncTask<Void, Game, Void> {
        /**
         * Runs in background off of the UI thread to avoid blocking screen activity.
         * Sends GETs to the /games/{gameId} service to update game.
         *
         * @return nothing
         */
        @Override
        protected Void doInBackground(Void... params) {

            while (true) {
                gameService.getGameFromNetwork(game.getId());
                if (isCancelled()) {
                    break;
                }
                SystemClock.sleep(1000);
            }
            return null;
        }
    }
}