package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.GameStatus;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Card;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.Toaster;

/**
 * A layout for the player hand.
 * Created by a-a-hofmann on 21/04/16.
 */
public class HandLayout extends LinearLayout implements View.OnClickListener {

    public interface OnCardClickedListener {
        void onCardClicked(CardView cView);
    }

    /**
     * Listeners waiting on onCardClicked events.
     */
    private OnCardClickedListener listener;

    private LinearLayout handLayout;

    private boolean isMyTurn;

    private boolean isAllowedToPlayACard;

    public HandLayout(Context context) {
        super(context);
        initializeViews(context);
    }

    public HandLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public HandLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }

    /**
     * Inflate xml layout file.
     *
     * @param context
     */
    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.hand_layout, this);

        handLayout = (LinearLayout) findViewById(R.id.YourDeck);
        isAllowedToPlayACard = true;
    }

    /**
     * Sets Cards.
     *
     * @param player Player, determines card color.
     */
    public void initCardImages(Player player) {
        handLayout.removeAllViews();
        updatePlayerHand(player, true, GameStatus.PLANNINGPHASE);
    }

    private void addCard(Character character, CardType type, boolean isMyTurn) {
        CardView card = new CardView(getContext());
        card.setCard(character, type);
        card.setOnClickListener(this);

        // Add new card to container.
        handLayout.addView(card);

        card.setEnabled(isMyTurn);
    }

    /**
     * On card clicked. Checks the lock on cards in case of double taps before actually playing a card.
     */
    public void onClick(View v) {
        CardView c = (CardView) v;
        if (CardType.BULLET.equals(c.getType())) {
            new Toaster(getContext())
                    .setText(getResources(), R.string.play_bullets)
                    .setLayout(R.layout.toast_custom)
                    .setDuration(Toast.LENGTH_SHORT)
                    .makeAToast().show();
        } else if (isMyTurn && listener != null && isAllowedToPlayACard) {
            // Notify listener that a card was played.
            listener.onCardClicked(c);

            isAllowedToPlayACard = false;
        }
    }

    public void setAllowedToPlayACard(boolean allowedToPlayACard) {
        this.isAllowedToPlayACard = allowedToPlayACard;
    }

    public void addOnCardClickedListener(OnCardClickedListener listener) {
        this.listener = listener;
    }

    public void updatePlayerHand(Player player, final boolean isMyTurn, GameStatus status) {
        List<Card> hand = player.getHand();
        handLayout.removeAllViews();

        // If action phase then disable hand.
        this.isMyTurn = !status.equals(GameStatus.ACTIONPHASE) && isMyTurn;

        for (Card c : hand) {
            addCard(player.getCharacter(), c.getType(), isMyTurn);
        }
    }
}
