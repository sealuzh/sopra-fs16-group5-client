package ch.uzh.ifi.seal.soprafs16.coltexpress.loaders;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.HashMap;
import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;

@SuppressWarnings("deprecation")
public class DocLoader extends ImageLoaderFactory implements ImageLoader {

    public DocLoader(Context context) {
        setContext(context);
    }

    @Override
    public Map<CardType, Drawable> getCards() {
        Map<CardType, Drawable> cards = new HashMap<>();
        cards.put(CardType.PUNCH, context().getResources().getDrawable(R.drawable.doc_punch_card));
        cards.put(CardType.ROBBERY, context().getResources().getDrawable(R.drawable.doc_collect_card));
        cards.put(CardType.FIRE, context().getResources().getDrawable(R.drawable.doc_shoot_card));
        cards.put(CardType.MOVE, context().getResources().getDrawable(R.drawable.doc_moveh_card));
        cards.put(CardType.FLOORCHANGE, context().getResources().getDrawable(R.drawable.doc_movev_card));
        cards.put(CardType.MARSHAL, context().getResources().getDrawable(R.drawable.doc_sherif_card));
        cards.put(CardType.BULLET, context().getResources().getDrawable(R.drawable.neutral_bullet_card));
        return cards;
    }

    @Override
    public Drawable getSpecialAbility() {
        return context().getResources().getDrawable(R.drawable.doc_abil);
    }

    @Override
    public Drawable getHalfPortrait() {
        return decodeSampledBitmapFromResource(context().getResources(), R.drawable.doc, 340, 458);
    }

    @Override
    public Drawable getEntirePortrait() {
        return decodeSampledBitmapFromResource(context().getResources(), R.drawable.doc_entire, 200, 488);
    }

    @Override
    public Drawable getMeeple() {
        return context().getResources().getDrawable(R.drawable.doc_meeple);
    }

    @Override
    public Drawable getCharacterCard() {
        return context().getResources().getDrawable(R.drawable.doc_abili);
    }
}