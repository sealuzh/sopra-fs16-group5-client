package ch.uzh.ifi.seal.soprafs16.coltexpress.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.GameStatus;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Game;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Loot;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;

/**
 * Utility methods for game activity.
 * Created by a-a-hofmann on 29/04/16.
 */
public class GameUtility {

    public interface OnTurnChangeListener {
        void onTurnChanged(Long turnId);
    }

    public interface OnGameFinishedListener {
        void onGameFinished();
    }

    public static int nrOfCars;

    private Game game;

    private Long playerId;

    private Player player;

    private Long roundId;

    private Long turnId;

    private boolean hasRoundChanged;

    private boolean hasPlayerBeenShot;

    private boolean hasPlayerBeenPunched;

    private OnTurnChangeListener onTurnChangeListener;

    private OnGameFinishedListener onGameFinishedListener;

    private List<Player> opponents;

    public GameUtility(Game game, Long playerId) {
        this.game = game;
        this.playerId = playerId;
        this.player = filterPlayer();
        this.opponents = filterOpponents();
        this.roundId = game.getRoundId();
        this.turnId = game.getTurnId();

        nrOfCars = game.getNrOfCars();
    }

    /**
     * Updates game model of game util.
     *
     * @param game Updated game
     */
    public void update(Game game) {
        this.game = game;

        if (isGameOver() && onGameFinishedListener != null) {
            onGameFinishedListener.onGameFinished();
        }

        hasRoundChanged = !game.getRoundId().equals(roundId);
        this.roundId = game.getRoundId();

        Long newTurnId = game.getTurnId();
        if (onTurnChangeListener != null && !newTurnId.equals(turnId)) {
            onTurnChangeListener.onTurnChanged(newTurnId);
            this.turnId = newTurnId;
        }

        Player updatedPlayer = filterPlayer();
        hasPlayerBeenShot = player.getInjuries() != updatedPlayer.getInjuries();
        hasPlayerBeenPunched = player.getBrokenNoses() != updatedPlayer.getBrokenNoses();

        player = updatedPlayer;
        opponents = filterOpponents();
    }

    /**
     * Gets own player.
     *
     * @return own player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets opponents.
     */
    public List<Player> getOpponents() {
        return opponents;
    }

    /**
     * Filters own player from list of game players.
     */
    private Player filterPlayer() {
        for (Player p : game.getPlayers()) {
            if (p.getId().equals(playerId)) {
                return p;
            }
        }

        return player;
    }

    /**
     * Gets a list of opponents from game players
     *
     * @return all opponents.
     */
    private List<Player> filterOpponents() {
        List<Player> opp = new ArrayList<>();

        for (Player p : game.getPlayers()) {
            if (!p.getId().equals(player.getId())) {
                opp.add(p);
            }
        }

        return opp;
    }

    /**
     * Checks if it is the players turn.
     */
    public boolean isMyTurn() {
        return player.getId().equals(game.getCurrentPlayerId());
    }

    /**
     * Checks if game is in action phase.
     */
    public boolean isActionPhase() {
        return game.getStatus().equals(GameStatus.ACTIONPHASE);
    }

    /**
     * Checks if the round has changed.
     */
    public boolean hasRoundChanged() {
        return hasRoundChanged;
    }

    /**
     * Checks if the player has been shot.
     */
    public boolean hasPlayerBeenShot() {
        return hasPlayerBeenShot;
    }

    /**
     * Checks if the player has been punched.
     */
    public boolean hasPlayerBeenPunched() {
        return hasPlayerBeenPunched;
    }

    private boolean isGameOver() {
        return GameStatus.FINISHED.equals(game.getStatus());
    }


    /**
     * Finds a player by id from game players list.
     *
     * @param id Id of the player we are looking for.
     * @return player with given id.
     */
    @Nullable
    public Player findById(Long id) {
        for (Player p : game.getPlayers()) {
            if (p.getId().equals(id)) {
                return p;
            }
        }

        return null;
    }

    /**
     * Gets winner(s) of this game.
     *
     * @return List of winners
     */
    public Player getWinner() {
        Player quitter = findById(game.getExitPlayerId());

        // Score container: id (key), score (value)
        Map<Long, Integer> scores = getScoresMap();

        // Get all gunslingers and add 1000 to their scores
        List<Player> gunSlingers = getGunslingers(quitter);
        for (Player p : gunSlingers) {
            scores.put(p.getId(), scores.get(p.getId()) + 1000);
        }

        // Get highscore for the game
        int highScore = getHighScore(scores, quitter);

        // Winners are all players that reached the highscore.
        List<Player> candidates = getPlayersWithHighScore(quitter, scores, highScore);

        // If there is a draw get the player that was shot the least.
        if (candidates.size() > 1) {
            Collections.sort(candidates, new Comparator<Player>() {
                @Override
                public int compare(Player lhs, Player rhs) {
                    return lhs.getInjuries() - rhs.getInjuries();
                }
            });
        }

        return candidates.get(0);
    }

    @NonNull
    private List<Player> getPlayersWithHighScore(Player quitter, Map<Long, Integer> scores, int highScore) {
        List<Player> winners = new ArrayList<>();
        for (Player p : game.getPlayers()) {
            if (scores.get(p.getId()).equals(highScore) && !p.equals(quitter)) {
                winners.add(p);
            }
        }
        return winners;
    }

    /**
     * Gets a map containing player's id as key and their score for the current game.
     *
     * @return Scores map.
     */
    @NonNull
    private Map<Long, Integer> getScoresMap() {
        Map<Long, Integer> scores = new HashMap<>();
        for (Player p : game.getPlayers()) {
            scores.put(p.getId(), getTotalLootsValue(p));
        }
        return scores;
    }

    /**
     * Gets the highest score for this game.
     *
     * @param scores Map containing all players and their scores.
     * @return the max score for this game.
     */
    private int getHighScore(Map<Long, Integer> scores, Player quitter) {
        int highScore = -1;

        Long quitterId = -1L;
        if (quitter != null) {
            quitterId = quitter.getId();
        }

        for (Map.Entry<Long, Integer> entry : scores.entrySet()) {
            int playerScore = entry.getValue();
            if (playerScore > highScore && !entry.getKey().equals(quitterId)) {
                highScore = playerScore;
            }
        }
        return highScore;
    }

    /**
     * Gets the sum of loot values for a player.
     *
     * @param player Player to compute sum of loots for.
     * @return total value of loots.
     */
    private int getTotalLootsValue(Player player) {
        int result = 0;
        List<Loot> loots = player.getLoots();

        for (Loot loot : loots) {
            result += loot.getValue();
        }

        return result;
    }

    /**
     * Gets the gunslinger(s) for the current game.
     * Players that shot the most bullets in a given game are awarded the gunslinger
     * prize: +1000 to their scores
     *
     * @return list of gunslingers
     */
    public List<Player> getGunslingers(Player quitter) {
        List<Player> players = game.getPlayers();

        // Ignore quitter
        List<Player> playersToSort = new ArrayList<>();
        for (Player p : players) {
            if (!p.equals(quitter)) {
                playersToSort.add(p);
            }
        }

        // Sort players (without the quitter) by number of bullets shot in game in ascending order
        Collections.sort(playersToSort, new Comparator<Player>() {
            @Override
            public int compare(Player lhs, Player rhs) {
                return lhs.getBullets() - rhs.getBullets();
            }
        });

        // After sorting, the first player should be the one that has the least amount of bullets
        // (shot the most bullets)
        int minBullets = playersToSort.get(0).getBullets();
        return getPlayersByNumberOfBullets(playersToSort, minBullets);
    }

    /**
     * Gets players by the number of bullets they have.
     *
     * @param players  List of players to search.
     * @param nBullets Number of bullets to search for.
     * @return All players that shot {@code nBullets}.
     */
    @NonNull
    private List<Player> getPlayersByNumberOfBullets(List<Player> players, int nBullets) {
        // Get all players that shot the same amount of bullets
        List<Player> results = new ArrayList<>();
        for (Player p : players) {
            if (p.getBullets() == nBullets) {
                results.add(p);
            }
        }

        return results;
    }

    @Nullable
    public Player getQuitter() {
        return findById(game.getExitPlayerId());
    }

    public void setOnTurnChangedListener(OnTurnChangeListener listener) {
        this.onTurnChangeListener = listener;
    }

    public void setOnGameFinishedListener(OnGameFinishedListener listener) {
        this.onGameFinishedListener = listener;
    }
}