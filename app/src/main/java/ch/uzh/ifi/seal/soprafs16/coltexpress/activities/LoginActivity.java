package ch.uzh.ifi.seal.soprafs16.coltexpress.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.PlayerService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.ReplaceFont;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.MusicPlayer;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity {

    @SuppressWarnings("unused")
    private Logger logger = Logger.getLogger(LoginActivity.class.getName());

    private MusicPlayer musicPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ReplaceFont.replaceDefaultFont(this, "DEFAULT", "CFOldMilwaukee-Regular.ttf");

        // Set up the login form.
        Button mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        musicPlayer = MusicPlayer.getInstance(this);
        musicPlayer.playOrResume(R.raw.for_a_few_dollars_more);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        PlayerService playerService = PlayerService.getInstance(this);
        final EditText mUsername = (EditText) findViewById(R.id.username);
        mUsername.setError(null);

        String username = mUsername.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(username)) {
            mUsername.setError(getString(R.string.error_invalid_username));
            focusView = mUsername;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            playerService.setOnPlayerCreatedListener(new PlayerService.PlayerCreatedListener() {
                @Override
                public void onPlayerCreated() {
                    musicPlayer.pause();
                    Intent intent = new Intent(LoginActivity.this, GameMenu.class);
                    startActivity(intent);
                }

                @Override
                public void onPlayerCreatedError() {
                    mUsername.setError(getString(R.string.network_error));
                }
            });
            playerService.createPlayer(username);
        }
    }
}