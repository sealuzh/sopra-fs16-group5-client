package ch.uzh.ifi.seal.soprafs16.coltexpress.utils;

import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.activities.GameActivity;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PossibilityDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.IMeepleLayout;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Game;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Level;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.popups.OnPossibilityChosenListener;

/**
 * Helper class to manipulate loot layouts for the entire train.
 * Created by a-a-hofmann on 22/04/16.
 */
public class MeepleLayoutUtility {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MeepleLayoutUtility.class.getSimpleName());

    /**
     * Number of meeplelayouts == number of cars
     */
    private int nrOfMeepleLayouts;

    /**
     * Game activity. Used for {@code findViewById}
     */
    private GameActivity activity;

    /**
     * Container of meeple layouts.
     */
    private List<Map<Level, IMeepleLayout>> meepleLayouts;

    private OnPossibilityChosenListener listener;

    public MeepleLayoutUtility(GameActivity activity, int nrOfMeepleLayouts) {
        this.activity = activity;
        this.nrOfMeepleLayouts = nrOfMeepleLayouts;

        meepleLayouts = new ArrayList<>();

        initViews();
    }

    /**
     * Find all views and initialize the layouts.
     */
    private void initViews() {
        // loco + 2 cars are always present
        meepleLayouts.add(createMap(
                (IMeepleLayout) activity.findViewById(R.id.meeple_car0),
                (IMeepleLayout) activity.findViewById(R.id.meeple_car0_top)));

        meepleLayouts.add(createMap(
                (IMeepleLayout) activity.findViewById(R.id.meeple_car1),
                (IMeepleLayout) activity.findViewById(R.id.meeple_car1_top)));

        meepleLayouts.add(createMap(
                (IMeepleLayout) activity.findViewById(R.id.meeple_car2),
                (IMeepleLayout) activity.findViewById(R.id.meeple_car2_top)));


        if (nrOfMeepleLayouts > 3) {
            meepleLayouts.add(createMap(
                    (IMeepleLayout) activity.findViewById(R.id.meeple_car3),
                    (IMeepleLayout) activity.findViewById(R.id.meeple_car3_top)));

            if (nrOfMeepleLayouts > 4) {
                meepleLayouts.add(createMap(
                        (IMeepleLayout) activity.findViewById(R.id.meeple_car4),
                        (IMeepleLayout) activity.findViewById(R.id.meeple_car4_top)));
            }
        }

        invalidate();
    }

    /**
     * Helper method to put inside the meepleLayouts container.
     *
     * @param bottom Bottom IMeepleLayout
     * @param top    Top IMeepleLayout
     * @return a map containing bottom and top IMeepleLayouts
     */
    private Map<Level, IMeepleLayout> createMap(IMeepleLayout bottom, IMeepleLayout top) {
        Map<Level, IMeepleLayout> meepleMapItem = new HashMap<>();
        meepleMapItem.put(Level.BOTTOM, bottom);
        meepleMapItem.put(Level.TOP, top);

        return meepleMapItem;
    }

    /**
     * Draws loots for each car, level, type.
     */
    public void drawMeeples(Game game) {
        // Deletes existing meeples
        invalidate();

        // Draw meeples
        for (Player p : game.getPlayers()) {
            meepleLayouts.get(p.getCar()).get(p.getLevel()).addMeeple(p.getCharacter());
        }

        meepleLayouts.get(game.getPositionMarshal()).get(Level.BOTTOM).addMeeple(Character.MARSHAL);
    }

    /**
     * Empties all layouts and stops all animations
     */
    public void invalidate() {
        for (Map<Level, IMeepleLayout> map : meepleLayouts) {
            map.get(Level.BOTTOM).emptyLayout();
            map.get(Level.BOTTOM).stopAnimation();
            map.get(Level.TOP).emptyLayout();
            map.get(Level.TOP).stopAnimation();
        }
    }

    /**
     * Draws possible moves.
     *
     * @param possibilities Possibility dto containing possible player positions.
     */
    public void drawMoves(final PossibilityDTO possibilities) {
        for (final Player p : possibilities.getPlayers()) {
            meepleLayouts.get(p.getCar()).get(p.getLevel()).addMoves();
            ((LinearLayout) meepleLayouts.get(p.getCar()).get(p.getLevel())).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<Player> players = new ArrayList<>();
                    players.add(p);

                    if (listener != null) {
                        meepleLayouts.get(p.getCar()).get(p.getLevel()).stopAnimation();
                        PossibilityDTO chosenPossibility = possibilities.toBuilder().players(players).build();
                        listener.onPossibilityChosen(chosenPossibility);
                    }
                }
            });
        }

    }

    /**
     * Draws possible marshal moves.
     *
     * @param possibilities Possibility dto containing possible marshal positions.
     */
    public void drawMarshalMoves(final PossibilityDTO possibilities) {
        for (final Integer car : possibilities.getMarshal()) {
            meepleLayouts.get(car).get(Level.BOTTOM).addMoves();
            ((LinearLayout) meepleLayouts.get(car).get(Level.BOTTOM)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<Integer> marshalChoice = new ArrayList<>();
                    marshalChoice.add(car);

                    if (listener != null) {
                        meepleLayouts.get(car).get(Level.BOTTOM).stopAnimation();
                        listener.onPossibilityChosen(possibilities.toBuilder().marshal(marshalChoice).build());
                    }
                }
            });
        }
    }

    public void addOnPossibilityChosenListener(OnPossibilityChosenListener listener) {
        this.listener = listener;
    }
}