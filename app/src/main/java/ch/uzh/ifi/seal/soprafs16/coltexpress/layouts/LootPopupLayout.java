package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.LootType;

/**
 * Created by francesca on 11.05.16.
 */
public class LootPopupLayout extends LinearLayout {

    private Integer purseNr;

    private Integer diamondNr;

    private Integer suitcaseNr;

    private TextView purseNrText;

    private TextView diamondNrText;

    private TextView suitcaseNrText;

    private ImageView purse;

    private ImageView diamond;

    private ImageView suitcase;

    private Button chooseButton;

    private ColorMatrixColorFilter bwFilter;

    private LootType chosenType;


    public LootPopupLayout(Context context) {
        super(context);
        initialize(context);
    }

    private void initialize(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.loot_popup_layout, this, true);

        purse = (ImageView) findViewById(R.id.purse);
        diamond = (ImageView) findViewById(R.id.diam);
        suitcase = (ImageView) findViewById(R.id.suitc);

        purseNrText = (TextView) findViewById(R.id.purseNum);
        diamondNrText = (TextView) findViewById(R.id.diamNum);
        suitcaseNrText = (TextView) findViewById(R.id.suitcaseNum);

        purse.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!purseNr.equals(0)) {
                    chosenType = LootType.PURSE;
                    setEnabled(true, purse);
                    setEnabled(false, diamond);
                    setEnabled(false, suitcase);

                    chooseButton.setEnabled(true);
                    chooseButton.setVisibility(VISIBLE);
                }
            }
        });

        diamond.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!diamondNr.equals(0)) {
                    chosenType = LootType.JEWEL;
                    setEnabled(true, diamond);
                    setEnabled(false, purse);
                    setEnabled(false, suitcase);

                    chooseButton.setEnabled(true);
                    chooseButton.setVisibility(VISIBLE);
                }
            }
        });

        suitcase.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!suitcaseNr.equals(0)) {
                    chosenType = LootType.STRONGBOX;
                    setEnabled(true, suitcase);
                    setEnabled(false, purse);
                    setEnabled(false, diamond);

                    chooseButton.setEnabled(true);
                    chooseButton.setVisibility(VISIBLE);
                }
            }
        });

        chooseButton = (Button) findViewById(R.id.choose);
        chooseButton.setClickable(true);
        chooseButton.setEnabled(false);
        chooseButton.setVisibility(INVISIBLE);

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        bwFilter = new ColorMatrixColorFilter(matrix);
    }

    public void setLootNumber(Map<LootType, Integer> loots) {
        purseNr = loots.get(LootType.PURSE);
        diamondNr = loots.get(LootType.JEWEL);
        suitcaseNr = loots.get(LootType.STRONGBOX);

        purseNrText.setText(purseNr.toString());
        diamondNrText.setText(diamondNr.toString());
        suitcaseNrText.setText(suitcaseNr.toString());

        if (purseNr.equals(0)) {
            setEnabled(false, purse);
        }
        if (diamondNr.equals(0)) {
            setEnabled(false, diamond);
        }
        if (suitcaseNr.equals(0)) {
            setEnabled(false, suitcase);
        }
    }


    public void setEnabled(boolean enabled, ImageView loot) {
        if (!enabled) {
            loot.setColorFilter(bwFilter);
        } else {
            loot.clearColorFilter();
        }
    }

    public Button getChooseButton() {
        return chooseButton;
    }

    public LootType getChosenType() {
        return chosenType;
    }
}