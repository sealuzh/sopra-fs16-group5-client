package ch.uzh.ifi.seal.soprafs16.coltexpress.service;

import java.util.List;

import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PlayerDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

public interface PlayersApiInterface {

    /**
     * Returns all users
     *
     * @param cb
     */
    @GET("/users")
    void getPlayers(Callback<List<Player>> cb);

    /**
     * Register a new user on the server
     */
    @POST("/users")
    void createPlayer(@Body PlayerDTO player, Callback<Player> cb);


}
