package ch.uzh.ifi.seal.soprafs16.coltexpress.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.GameStatus;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.GameDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.fragments.GameCreateFragment;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Game;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.GameService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.PlayerService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.ReplaceFont;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.MusicPlayer;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.Toaster;

/**
 * Handles the options screen.
 * The user can either choose to getInstance a new game or join an existing one.
 */
public class GameMenu extends Activity implements
        GameCreateFragment.GameCreateDialogListener, GameService.GameCreatedListener {

    private static final Logger logger = Logger.getLogger(GameMenu.class.getName());

    private static final String FROM1 = "gameName";

    private static final String FROM2 = "owner";

    private static final String FROMID = "gameId";

    private static final String[] from = {FROM1, FROM2};

    private static final int[] to = {android.R.id.text1, android.R.id.text2};

    private GameListReceivedRefresher gameListRefresher;

    private GameService gameService;

    private PlayerService playerService;

    private MusicPlayer musicPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_options);
        ReplaceFont.replaceDefaultFont(this, "DEFAULT", "CFOldMilwaukee-Regular.ttf");
        getActionBar().setDisplayHomeAsUpEnabled(true);

        playerService = PlayerService.getInstance(this);
        musicPlayer = MusicPlayer.getInstance(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        gameListRefresher.cancel(true);
        gameListRefresher = null;
        musicPlayer.pause();
        gameService.removeOnGameCreatedListener();
    }

    @Override
    protected void onResume() {
        super.onResume();

        musicPlayer.playOrResume(R.raw.for_a_few_dollars_more);
        initializeServices();
        initializeUI();
        gameListRefresher = new GameListReceivedRefresher();
        gameListRefresher.execute();
    }

    /**
     * Initialize services.
     */
    private void initializeServices() {
        gameService = GameService.getInstance(this);
        playerService = PlayerService.getInstance(this);
    }

    /**
     * Initialize username UI.
     */
    private void initializeUserNameText() {
        final TextView usernameText = (TextView) findViewById(R.id.logged_in_as_text);

        // When a player is created: guest or actual player, set username text.
        playerService.setOnPlayerCreatedListener(new PlayerService.PlayerCreatedListener() {
            @Override
            public void onPlayerCreated() {
                String text = getString(R.string.logged_in_as) + playerService.getPlayer().getUsername();
                usernameText.setText(text);

                new Toaster(getBaseContext())
                        .setText(text)
                        .setLayout(R.layout.toast_custom)
                        .setGravity(Gravity.CENTER)
                        .setDuration(Toast.LENGTH_SHORT)
                        .makeAToast().show();
            }

            @Override
            public void onPlayerCreatedError() {
                logger.severe("Failed to getInstance user!");
            }
        });

        // If player is not logged in getInstance guest player.
        if (playerService.isGuest()) {
            playerService.createGuestPlayer();
        } else {
            String text = getString(R.string.logged_in_as) + playerService.getPlayer().getUsername();
            usernameText.setText(text);

            new Toaster(getBaseContext())
                    .setText(text)
                    .setLayout(R.layout.toast_custom)
                    .setGravity(Gravity.CENTER)
                    .makeAToast()
                    .show();
        }
    }

    private void initializeGameList() {
        // List UI component
        final ListView listView = (ListView) findViewById(R.id.gameList);

        // When a list item is clicked, getInstance a game object to use for future requests
        // and go to character activity.
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Map<String, String> clickedItem = (Map<String, String>) listView.getAdapter().getItem(position);

                Long gameId = Long.parseLong(clickedItem.get(FROMID));

                // Get info regarding chosen game and go to character activity.
                gameService.setOnGameReceivedListener(new GameService.GameReceivedListener() {
                    @Override
                    public void onGameReceived(Game result) {
                        openCharacters();
                    }
                });
                gameService.getGameFromNetwork(gameId);
            }
        });
    }

    /**
     * Initialize UI buttons:
     * 1) getInstance new game button
     * 2) change username button
     * 3) demoTarget button
     */
    private void initializeButtons() {
        // Game getInstance dialog
        final GameCreateFragment gameCreateFragment = new GameCreateFragment();
        gameCreateFragment.setOnGameCreateDialogClickListener(this);

        // When newGameButton is clicked demo newGameFragment dialog
        Button newGameButton = (Button) findViewById(R.id.button);
        newGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameCreateFragment.show(getFragmentManager(), "createGame");
            }
        });

        // username change button
        Button changeUsernameButton = (Button) findViewById(R.id.sign_in_button);
        changeUsernameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GameMenu.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Initialize all UI elements.
     */
    private void initializeUI() {
        initializeGameList();
        initializeButtons();
        initializeUserNameText();
    }

    /**
     * Open characters activity.
     */
    private void openCharacters() {
        Intent intent = new Intent(this, CharactersActivity.class);
        startActivity(intent);
    }

    @Override
    public void onGameCreateDialog(String gameName) {
        Player player = playerService.getPlayer();

        GameDTO game = GameDTO.create(gameName);

        gameService.setOnGameCreatedListener(this);
        gameService.createGame(game, player);
    }

    @Override
    public void onGameCreated() {
        Intent intent = new Intent(getApplicationContext(), CharactersActivity.class);
        startActivity(intent);
    }

    /**
     * The class {@code GameList} handles the network calls to refresh the list of open games.
     */
    public class GameListReceivedRefresher extends AsyncTask<Void, Void, Void>
            implements GameService.GameListReceivedListener {

        private SimpleAdapter adapter;

        private List<Map<String, String>> gameList;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            gameService.setOnGameListReceivedListener(this);

            gameList = new ArrayList<>();

            final ListView listView = (ListView) findViewById(R.id.gameList);

            adapter = new SimpleAdapter(GameMenu.this, gameList, android.R.layout.simple_list_item_2, from, to);

            listView.setAdapter(adapter);
        }

        /**
         * Every time {@code publishProgress()} is called, onProgressUpdate is called on the UI
         * thread updating the list of games.
         */
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            adapter.notifyDataSetChanged();
        }

        /**
         * Runs in background off of the UI thread to avoid blocking screen activity.
         * Sends GETs to the /games service to update list of open games.
         *
         * @return nothing. It is void because Asynctask requires it.
         */
        @Override
        protected Void doInBackground(Void... params) {
            while (true) {
                gameService.getGames();

                if (isCancelled()) {
                    gameService.removeOnGameListReceivedListener();
                    break;
                }

                SystemClock.sleep(2000);
            }

            return null;
        }

        /**
         * Inserts game data into a map for the Listview adapter.
         */
        private Map<String, String> putData(Game gameToBeInserted) {
            HashMap<String, String> item = new HashMap<>();
            item.put(FROMID, gameToBeInserted.getId().toString());
            item.put(FROM1, gameToBeInserted.getName());
            item.put(FROM2, gameToBeInserted.getOwner());
            return item;
        }

        @Override
        public void onGameListReceived(List<Game> games) {
            for (Game game : games) {
                if (game.getStatus().equals(GameStatus.PENDING) && !isGameInList(game)) {
                    gameList.add(putData(game));
                }
            }

            publishProgress();
        }

        /**
         * Checks if a game is already in the game list.
         */
        private boolean isGameInList(Game game) {
            for (Map<String, String> gameMap : gameList) {
                Long gameId = Long.parseLong(gameMap.get(FROMID));
                if (gameId.equals(game.getId())) {
                    return true;
                }
            }
            return false;
        }
    }
}
