package ch.uzh.ifi.seal.soprafs16.coltexpress.service;

import android.content.Context;

import java.util.UUID;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PlayerDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * The {@code PlayerService} class handles the creation of players.
 * Created by alexanderhofmann on 09/04/16.
 */
public class PlayerService {

    public interface PlayerCreatedListener {
        void onPlayerCreated();

        void onPlayerCreatedError();
    }

    private static final Logger logger = Logger.getLogger(PlayerService.class.getName());

    /**
     * Singleton instance.
     */
    private static PlayerService playerService;

    /**
     * Cached player.
     */
    private Player player;

    /**
     * Context. For Retrofit.
     */
    private Context context;

    /**
     * Listener for player creation.
     */
    private PlayerCreatedListener playerCreatedListener;

    /**
     * Private constructor to avoid instantiation.
     */
    private PlayerService() {
    }

    /**
     * Get instance of singleton class.
     *
     * @return Instance of this class.
     */
    public static PlayerService getInstance(Context context) {
        if (playerService == null) {
            playerService = new PlayerService();
        }

        playerService.context = context;
        return playerService;
    }

    /**
     * Gets cached player.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Sets cached player.
     *
     * @param player Player to be cached.
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * Creates a new player.
     *
     * @param username Username of the player to be created.
     */
    public void createPlayer(String username) {
        PlayerDTO playerDTO = PlayerDTO.create(username);
        RestService.getPlayersApiInstance(context).createPlayer(playerDTO, new Callback<Player>() {
            @Override
            public void success(Player playerResponse, Response response) {
                player = playerResponse;
                if (playerCreatedListener != null) {
                    playerCreatedListener.onPlayerCreated();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logError(error);
                if (playerCreatedListener != null) {
                    playerCreatedListener.onPlayerCreatedError();
                }
            }
        });
    }

    /**
     * Creates a guest player. Guest players are given a random username.
     */
    public void createGuestPlayer() {
        String username = "guest-" + UUID.randomUUID().toString().substring(0, 8);
        createPlayer(username);
    }

    /**
     * Checks if user has no player yet.
     *
     * @return {@code true} iff user has no player, {@code false} otherwise.
     */
    public boolean isGuest() {
        return player == null;
    }

    /**
     * Logs retrofit errors
     *
     * @param error Error object.
     */
    private void logError(RetrofitError error) {
        logger.severe(error.getUrl());
        logger.severe(error.getMessage());
    }

    /**
     * Sets the listener for player creation event.
     *
     * @param listener Listener for event.
     */
    public void setOnPlayerCreatedListener(PlayerCreatedListener listener) {
        playerCreatedListener = listener;
    }
}