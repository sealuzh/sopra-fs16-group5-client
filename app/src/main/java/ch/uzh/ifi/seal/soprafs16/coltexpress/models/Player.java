package ch.uzh.ifi.seal.soprafs16.coltexpress.models;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.util.List;

import auto.parcel.AutoParcel;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.gson.AutoGson;

/**
 * Created by a-a-hofmann on 11/04/16.
 */
@AutoGson
@AutoParcel
public abstract class Player implements Parcelable {

    public abstract Long getId();

    public abstract String getUsername();

    public abstract String getToken();

    @Nullable
    public abstract Character getCharacter();

    public abstract int getCar();

    @Nullable
    public abstract Level getLevel();

    public abstract int getBullets();

    public abstract List<Loot> getLoots();

    public abstract List<Card> getHand();

    public abstract int getInjuries();

    public abstract int getBrokenNoses();


    public static Player create(Long id,
                                int car,
                                @Nullable Level level,
                                String username,
                                String token,
                                @Nullable Character character,
                                int bullets,
                                List<Loot> loots,
                                List<Card> hand,
                                int injuries,
                                int brokenNoses) {
        return builder()
                .id(id)
                .username(username)
                .token(token)
                .character(character)
                .car(car)
                .level(level)
                .bullets(bullets)
                .loots(loots)
                .hand(hand)
                .injuries(injuries)
                .brokenNoses(brokenNoses)
                .build();
    }

    public static Builder builder() {
        return new AutoParcel_Player.Builder();
    }

    @AutoParcel.Builder
    public interface Builder {
        Builder id(Long id);

        Builder username(String username);

        Builder token(String token);

        Builder character(Character character);

        Builder car(int car);

        Builder level(Level level);

        Builder bullets(int bullets);

        Builder loots(List<Loot> loots);

        Builder hand(List<Card> hand);

        Builder injuries(int injuries);

        Builder brokenNoses(int brokenNoses);

        Player build();
    }
}