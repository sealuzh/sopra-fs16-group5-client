package ch.uzh.ifi.seal.soprafs16.coltexpress.dtos;

import auto.parcel.AutoParcel;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.gson.AutoGson;

/**
 * Created by a-a-hofmann on 30/04/16.
 */
@AutoGson
@AutoParcel
public abstract class PlayerDTO {
    public abstract String username();

    public static PlayerDTO create(String username) {
        return new AutoParcel_PlayerDTO(username);
    }
}