package ch.uzh.ifi.seal.soprafs16.coltexpress.service;


import java.util.List;

import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.GameDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Game;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by alexanderhofmann on 06/04/16.
 */
public interface GamesApiInterface {
    /**
     * Returns all games
     */
    @GET("/games?filter=AVAILABLE")
    void getGames(Callback<List<Game>> cb);

    @POST("/games")
    void createGame(@Body GameDTO game, @Query("token") String token, Callback<Game> cb);

    @GET("/games/{gameId}")
    void getGame(@Path("gameId") Long gameId, Callback<Game> cb);

    @GET("/games/{gameId}/players")
    void getPlayers(@Path("gameId") Long id, Callback<List<Player>> cb);

    @POST("/games/{gameId}/players")
    void joinGame(@Path("gameId") Long id, @Query("token") String token, @Query("character") Character character, Callback<Player> cb);

    @PUT("/games/{gameId}/players")
    void joinGameAsOwner(@Path("gameId") Long id, @Query("token") String token, @Query("character") Character character, Callback<Player> cb);

    @POST("/games/{gameId}/start")
    void startGame(@Path("gameId") Long id, @Query("token") String token, Callback<Void> cb);

    @POST("/games/{gameId}/fasttrack")
    void startQuickGame(@Path("gameId") Long id, @Query("token") String token, Callback<Void> cb);

    @PUT("/games/{gameId}/exit")
    void exitGame(@Path("gameId") Long id, @Query("token") String token, Callback<Void> cb);
}
