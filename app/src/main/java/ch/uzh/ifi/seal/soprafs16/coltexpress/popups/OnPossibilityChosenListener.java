package ch.uzh.ifi.seal.soprafs16.coltexpress.popups;

import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PossibilityDTO;

/**
 * Created by a-a-hofmann on 13/05/16.
 */
public interface OnPossibilityChosenListener {
    void onPossibilityChosen(PossibilityDTO possibilityChosen);
}
