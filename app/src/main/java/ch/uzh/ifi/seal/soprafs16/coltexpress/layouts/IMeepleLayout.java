package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;

/**
 * Layout interface for meeple and meepleloc layouts.
 * Created by a-a-hofmann on 26/04/16.
 */
public interface IMeepleLayout {

    void addMeeple(Character character);

    void addMoves();

    void emptyLayout();

    void stopAnimation();
}
