package ch.uzh.ifi.seal.soprafs16.coltexpress.constants;

/**
 * Types of a turn in a round.
 * <p/>
 * Created by soyabeen on 24.03.16.
 */
public enum Turn {
    NORMAL("Normal"),
    HIDDEN("Hidden"),
    DOUBLE_TURNS("Double turns"),
    REVERSE("Reverse");

    private String turnString;

    Turn(String s) {
        turnString = s;
    }

    public String getTurnString() {
        return turnString;
    }
}