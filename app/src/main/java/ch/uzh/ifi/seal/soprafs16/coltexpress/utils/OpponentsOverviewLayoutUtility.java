package ch.uzh.ifi.seal.soprafs16.coltexpress.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.activities.GameActivity;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.OtherPlayerOverviewLayout;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Loot;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.LootType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;

/**
 * The {@code OpponentsOverviewLayoutUtility} handles all opponents overview and makes it easier
 * to update them.
 * Created by a-a-hofmann on 29/04/16.
 */
public class OpponentsOverviewLayoutUtility {

    private static final Logger logger = Logger.getLogger(OpponentsOverviewLayoutUtility.class.getSimpleName());

    private List<OtherPlayerOverviewLayout> opponentLayouts;

    /**
     * Constructor
     *
     * @param activity      To search for overview ids
     * @param nrOfOpponents Nr of overviews to initialize
     */
    public OpponentsOverviewLayoutUtility(GameActivity activity, int nrOfOpponents) {
        opponentLayouts = new ArrayList<>();

        switch (nrOfOpponents) {
            case 3:
                opponentLayouts.add((OtherPlayerOverviewLayout) activity.findViewById(R.id.player4));
            case 2:
                opponentLayouts.add((OtherPlayerOverviewLayout) activity.findViewById(R.id.player3));
            case 1:
                opponentLayouts.add((OtherPlayerOverviewLayout) activity.findViewById(R.id.player2));
                break;
            default:
                logger.severe("Number of players invalid");
        }
    }

    /**
     * Update opponents overview
     *
     * @param opponents Opponents for update.
     */
    public void update(List<Player> opponents, Long currentPlayerId) {
        if (opponents.size() == opponentLayouts.size()) {
            int i = 0;
            for (Player p : opponents) {
                Map<LootType, Integer> map = getLootMap(p);
                opponentLayouts.get(i).setOpponent(
                        p.getCharacter(),
                        p.getUsername(),
                        map.get(LootType.PURSE),
                        map.get(LootType.JEWEL),
                        map.get(LootType.STRONGBOX),
                        p.getBullets());

                if (p.getId().equals(currentPlayerId)) {
                    opponentLayouts.get(i).setHighlighted(true);
                } else {
                    opponentLayouts.get(i).setHighlighted(false);
                }
                i++;
            }
        } else {
            logger.severe("Number of opponents (" + opponents.size() + ") and number of" +
                    "layouts (" + opponentLayouts.size() + ") do not match!");
        }
    }

    /**
     * Counts how many loots the opponent have.
     *
     * @param p Player.
     * @return Mapping of loot types and quantity of loot.
     */
    private Map<LootType, Integer> getLootMap(Player p) {
        Map<LootType, Integer> result = new HashMap<>();

        int purse = 0;
        int jewel = 0;
        int strongbox = 0;

        List<Loot> loots = p.getLoots();
        if (loots != null) {

            for (Loot l : p.getLoots()) {

                switch (l.getType()) {
                    case PURSE_BIG:
                    case PURSE_SMALL:
                    case PURSE:
                        purse++;
                        break;

                    case JEWEL:
                        jewel++;
                        break;

                    case STRONGBOX:
                        strongbox++;
                        break;
                    default:
                        logger.severe("LootType not recognized");
                        logger.severe(l.toString());
                }
            }
        } else {
            logger.severe("Loots for player are null!");
        }

        result.put(LootType.PURSE, purse);
        result.put(LootType.JEWEL, jewel);
        result.put(LootType.STRONGBOX, strongbox);

        return result;
    }
}



