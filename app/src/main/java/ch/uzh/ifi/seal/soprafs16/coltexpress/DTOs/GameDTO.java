package ch.uzh.ifi.seal.soprafs16.coltexpress.dtos;

import auto.parcel.AutoParcel;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.gson.AutoGson;

/**
 * Created by a-a-hofmann on 30/04/16.
 */
@AutoGson
@AutoParcel
public abstract class GameDTO {

    public abstract String getName();

    public static GameDTO create(String name) {
        return new AutoParcel_GameDTO(name);
    }
}
