package ch.uzh.ifi.seal.soprafs16.coltexpress.models;

/**
 * Created by a-a-hofmann on 19/04/16.
 */
public enum LootType {
    PURSE, PURSE_SMALL, PURSE_BIG, JEWEL, STRONGBOX
}
