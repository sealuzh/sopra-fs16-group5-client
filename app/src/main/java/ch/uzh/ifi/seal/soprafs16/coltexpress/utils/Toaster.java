package ch.uzh.ifi.seal.soprafs16.coltexpress.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;

/**
 * Created by Selina on 20.05.2016.
 */
public class Toaster {

    private View layout;

    private TextView txt;

    private String text;

    private int gravity = Gravity.BOTTOM;

    private int duration = Toast.LENGTH_LONG;

    private Drawable background;

    private Context context;

    public Toaster(Context context) {
        this.context = context;
    }

    public Toaster setLayout(int layoutId) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(layoutId, null);

        txt = (TextView) layout.findViewById(R.id.textToShow);

        return this;
    }

    public Toaster setText(String text) {
        this.text = text;
        return this;
    }

    public Toaster setText(Resources res, int rid) {
        this.text = res.getString(rid);
        return this;
    }

    public Toaster setGravity(int gravity) {
        this.gravity = gravity;
        return this;
    }

    public Toaster setDuration(int duration) {
        this.duration = duration;
        return this;
    }

    public Toaster setBackground(int backgroundId) {
        return setBackgroundDrawable(context.getResources().getDrawable(backgroundId));
    }

    public Toaster setBackgroundDrawable(Drawable backgroundDrawable) {
        background = backgroundDrawable;
        return this;
    }

    public Toast makeAToast() {
        Toast toast = new Toast(context);

        if (layout != null) {
            txt = (TextView) layout.findViewById(R.id.textToShow);
            if (background != null) {
                txt.setBackground(background);
            }
            toast.setView(layout);
        }
        /**
         * In case of the roundend the text should be displayed
         * on another textview than the background
         */
        if (text != null && layout != null) {
            if (layout.getId() == R.id.toast_roundend) {
                txt = (TextView) layout.findViewById(R.id.textToShow2);
            }
            txt.setText(text);
        }

        toast.setGravity(gravity, 0, 0);

        toast.setDuration(duration);

        return toast;
    }
}
