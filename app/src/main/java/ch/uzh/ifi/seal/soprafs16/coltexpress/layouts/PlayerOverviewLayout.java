package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.GameStatus;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoader;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoaderFactory;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.LootType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.LootUtility;
import it.sephiroth.android.library.tooltip.Tooltip;

/**
 * Created by francesca on 22.04.16.
 */
public class PlayerOverviewLayout extends LinearLayout {

    public interface OnDrawClickListener {
        void onDrawClicked();
    }

    private ImageView myCharacter;
    private TextView textUsername;
    private TextView textBag;
    private TextView textDiamond;
    private TextView textSuitcase;
    private ImageView bullet;
    private Button drawButton;
    private Map<Character, String> abilityDescriptionMap;

    private OnDrawClickListener onDrawListener;

    public PlayerOverviewLayout(Context context) {
        super(context);
        initializeViews(context);
    }

    public PlayerOverviewLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public PlayerOverviewLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.player_overview_layout, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        setBackground(getResources().getDrawable(R.drawable.border));

        myCharacter = (ImageView) findViewById(R.id.myCharac);

        textUsername = (TextView) findViewById(R.id.myUsername);

        textBag = (TextView) findViewById(R.id.my_bag);

        textDiamond = (TextView) findViewById(R.id.my_diamond);

        textSuitcase = (TextView) findViewById(R.id.my_suitcase);

        bullet = (ImageView) findViewById(R.id.myBulletsPic);

        drawButton = (Button) findViewById(R.id.draw);

        initializeDrawButton();
    }

    public void addOnDrawClickListener(OnDrawClickListener listener) {
        onDrawListener = listener;
    }

    public void setBullets(int bullets) {
        // bullets correspond to the number of bullets a player could still shoot
        if (bullets == 0) {
            bullet.setImageDrawable(getContext().getResources().getDrawable(R.drawable.roundhighlight));
        } else {
            bullet.setImageDrawable(ImageLoaderFactory.getBulletPic(getContext(), bullets));
        }
    }

    public void setMyOverview(final Character character, String username,
                              Integer purses, Integer jewels, Integer suitcases, Integer bullets) {

        ImageLoader loader = ImageLoaderFactory.createLoader(getContext(), character);
        myCharacter.setImageDrawable(loader.getCharacterCard());
        myCharacter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showAbilDescription(character);
            }
        });

        abilityDescriptionMap = new HashMap<>();
        abilityDescriptionMap.put(Character.BELLE,
                getContext().getString(R.string.belle_ability_desc));

        abilityDescriptionMap.put(Character.CHEYENNE,
                getContext().getString(R.string.cheyenne_ability_desc));

        abilityDescriptionMap.put(Character.DJANGO,
                getContext().getString(R.string.django_ability_desc));

        abilityDescriptionMap.put(Character.TUCO,
                getContext().getString(R.string.tuco_ability_desc));

        abilityDescriptionMap.put(Character.DOC,
                getContext().getString(R.string.doc_ability_desc));

        abilityDescriptionMap.put(Character.GHOST,
                getContext().getString(R.string.ghost_ability_desc));

        textUsername.setText(username);
        // Set player loots
        textBag.setText(purses.toString());
        textDiamond.setText(jewels.toString());
        textSuitcase.setText(suitcases.toString());

        setBullets(bullets);
    }

    public void setMyOverview(Player player) {
        setMyOverview(player.getCharacter(), player.getUsername(), 0, 0, 0, player.getBullets());
        setLoots(player);
    }

    public void initializeDrawButton() {
        drawButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                onDrawListener.onDrawClicked();
            }
        });
    }

    public void setLoots(Player p) {
        Map<LootType, Integer> loots = LootUtility.getLootValueMap(p);

        textBag.setText(loots.get(LootType.PURSE).toString());
        textDiamond.setText(loots.get(LootType.JEWEL).toString());
        textSuitcase.setText(loots.get(LootType.STRONGBOX).toString());
    }

    public void setEnabled(boolean enabled, GameStatus status) {
        super.setEnabled(enabled);
        if (status.equals(GameStatus.PLANNINGPHASE)) {
            drawButton.setEnabled(enabled);
        } else {
            drawButton.setEnabled(false);
        }
    }

    public void setHighlighted(boolean isHighlighted) {
        Drawable border = getBackground();
        if (isHighlighted) {
            border.setAlpha(255);
        } else {
            border.setAlpha(0);
        }
    }

    public void showAbilDescription(Character character) {
        Tooltip.make(getContext(),
                new Tooltip.Builder(101)
                        .anchor(myCharacter, Tooltip.Gravity.LEFT)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 10000)
                        .activateDelay(800)
                        .showDelay(300)
                        .text(abilityDescriptionMap.get(character))
                        .maxWidth(500)
                        .withArrow(true)
                        .withOverlay(false)
                        .withStyleId(R.style.ToolTipLayoutCustomStyle)
                        .floatingAnimation(Tooltip.AnimationBuilder.SLOW)
                        .build()
        ).show();
    }
}