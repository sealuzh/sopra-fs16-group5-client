package ch.uzh.ifi.seal.soprafs16.coltexpress.loaders;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;

@SuppressWarnings("deprecation")
public class MarshalLoader extends ImageLoaderFactory implements ImageLoader {

    public MarshalLoader(Context context) {
        setContext(context);
    }

    @Override
    public Map<CardType, Drawable> getCards() {
        return null;
    }

    @Override
    public Drawable getSpecialAbility() {
        return null;
    }

    @Override
    public Drawable getHalfPortrait() {
        return null;
    }

    @Override
    public Drawable getMeeple() {
        return context().getResources().getDrawable(R.drawable.m_meeple);
    }

    @Override
    public Drawable getEntirePortrait() {
        return null;
    }

    @Override
    public Drawable getCharacterCard() {
        return null;
    }
}