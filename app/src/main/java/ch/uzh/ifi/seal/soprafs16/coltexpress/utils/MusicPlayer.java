package ch.uzh.ifi.seal.soprafs16.coltexpress.utils;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by alex on 21.05.2016.
 */
public class MusicPlayer {

    private MediaPlayer mediaPlayer;

    private int msCount;

    private Context context;

    private int songPlaying;

    private MusicPlayer() {
    }

    public static MusicPlayer getInstance(Context context) {
        MusicPlayer musicPlayer = new MusicPlayer();
        musicPlayer.context = context;

        return musicPlayer;
    }

    public void playOrResume(int songToResumeOrStart) {
        if (mediaPlayer != null && songToResumeOrStart == songPlaying) {
            resume();
        } else {
            play(songToResumeOrStart);
        }
    }

    private void play(int songId) {
        setSongPlaying(songId);
        mediaPlayer = MediaPlayer.create(context, songId);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    public void setSongPlaying(int songPlaying) {
        this.songPlaying = songPlaying;
    }

    public void pause() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            msCount = mediaPlayer.getCurrentPosition();
        }
    }

    private void resume() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
            mediaPlayer.seekTo(msCount);
        }
    }

    public void close() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}
