package ch.uzh.ifi.seal.soprafs16.coltexpress.loaders;

import android.graphics.drawable.Drawable;

import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;

/**
 * Created by alex on 17.05.2016.
 */
public interface ImageLoader {

    Map<CardType, Drawable> getCards();

    Drawable getSpecialAbility();

    Drawable getHalfPortrait();

    Drawable getEntirePortrait();

    Drawable getMeeple();

    Drawable getCharacterCard();
}
