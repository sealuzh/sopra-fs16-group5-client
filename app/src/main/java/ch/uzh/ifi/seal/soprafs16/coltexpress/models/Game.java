package ch.uzh.ifi.seal.soprafs16.coltexpress.models;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.util.List;

import auto.parcel.AutoParcel;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.GameStatus;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.gson.AutoGson;

/**
 * Created by a-a-hofmann on 13/04/16.
 */
@AutoGson
@AutoParcel
public abstract class Game implements Parcelable {

    public abstract Long getId();

    public abstract String getName();

    public abstract String getOwner();

    public abstract GameStatus getStatus();

    @Nullable
    public abstract Long getCurrentPlayerId();

    public abstract Integer getNrOfCars();

    public abstract List<Player> getPlayers();

    public abstract List<Loot> getLoots();

    public abstract Long getRoundId();

    public abstract Long getTurnId();

    @Nullable
    public abstract Card getCurrentCard();

    public abstract int getPositionMarshal();

    @Nullable
    public abstract Long getExitPlayerId();

    public static Game create(Long id,
                              String name,
                              String owner,
                              GameStatus status,
                              @Nullable Long currentPlayerId,
                              Integer nrOfCars,
                              List<Player> players,
                              List<Loot> loots,
                              Long roundId,
                              Long turnId,
                              @Nullable Card currentCard,
                              int positionMarshal,
                              @Nullable Long exitPlayerId) {
        return builder()
                .id(id)
                .name(name)
                .owner(owner)
                .status(status)
                .currentPlayerId(currentPlayerId)
                .nrOfCars(nrOfCars)
                .players(players)
                .loots(loots)
                .roundId(roundId)
                .turnId(turnId)
                .currentCard(currentCard)
                .positionMarshal(positionMarshal)
                .exitPlayerId(exitPlayerId)
                .build();
    }

    public static Builder builder() {
        return new AutoParcel_Game.Builder();
    }

    public abstract Builder toBuilder();

    @AutoParcel.Builder
    public interface Builder {
        Builder id(Long id);

        Builder name(String username);

        Builder owner(String owner);

        Builder status(GameStatus status);

        Builder currentPlayerId(Long currentPlayerId);

        Builder nrOfCars(Integer nrOfCars);

        Builder players(List<Player> players);

        Builder loots(List<Loot> loots);

        Builder roundId(Long roundId);

        Builder turnId(Long turnId);

        Builder currentCard(Card currentCard);

        Builder positionMarshal(int positionMarshal);

        Builder exitPlayerId(Long exitPlayerId);

        Game build();
    }
}