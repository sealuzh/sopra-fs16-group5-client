package ch.uzh.ifi.seal.soprafs16.coltexpress.models;

import android.os.Parcelable;

import auto.parcel.AutoParcel;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.gson.AutoGson;

/**
 * Created by a-a-hofmann on 29/04/16.
 */
@AutoGson
@AutoParcel
public abstract class Card implements Parcelable {
    public abstract Long getId();

    public abstract CardType getType();

    public abstract Long getOwnerId();

    public abstract boolean getOnHand();

    public abstract boolean getFaceDown();


    public static Card create(Long id, CardType type, Long ownerId, boolean onHand, boolean isFaceDown) {
        return builder()
                .id(id)
                .type(type)
                .ownerId(ownerId)
                .onHand(onHand)
                .faceDown(isFaceDown)
                .build();
    }

    public static Builder builder() {
        return new AutoParcel_Card.Builder();
    }

    @AutoParcel.Builder
    public interface Builder {
        Builder id(Long id);

        Builder type(CardType type);

        Builder ownerId(Long ownerId);

        Builder onHand(boolean onHand);

        Builder faceDown(boolean isFaceDown);

        Card build();
    }
}