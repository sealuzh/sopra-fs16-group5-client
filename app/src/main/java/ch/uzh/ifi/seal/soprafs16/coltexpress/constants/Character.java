package ch.uzh.ifi.seal.soprafs16.coltexpress.constants;

/**
 * Created by alexanderhofmann on 22/03/16.
 */
public enum Character {
    GHOST, BELLE, CHEYENNE, TUCO, DJANGO, DOC, MARSHAL
}