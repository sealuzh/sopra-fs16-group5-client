package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoaderFactory;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.LootType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.LootUtility;

/**
 * Created by francesca on 12.05.16.
 */
public class EndPlayerLayout extends LinearLayout {

    private ImageView figure;

    private ImageView shooterPrize;

    private TextView username;

    private TextView purses;

    private TextView diamonds;

    private TextView suitcases;

    private TextView total;

    private LinearLayout endPlayer;

    public EndPlayerLayout(Context context) {
        super(context);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View endPlayerLayout = inflater.inflate(R.layout.end_player_layout, this, true);


        username = (TextView) endPlayerLayout.findViewById(R.id.name);
        figure = (ImageView) endPlayerLayout.findViewById(R.id.figure);
        purses = (TextView) endPlayerLayout.findViewById(R.id.my_bag);
        diamonds = (TextView) endPlayerLayout.findViewById(R.id.my_diamond);
        suitcases = (TextView) endPlayerLayout.findViewById(R.id.my_suitcase);
        shooterPrize = (ImageView) endPlayerLayout.findViewById(R.id.best_shooter);
        total = (TextView) endPlayerLayout.findViewById(R.id.total);
        endPlayer = (LinearLayout) endPlayerLayout.findViewById(R.id.end_player);
    }

    public void setInfo(Player player, Boolean isBestShooter) {
        // username
        username.setText(player.getUsername());

        // figure
        figure.setImageDrawable(ImageLoaderFactory.createLoader(getContext(), player.getCharacter()).getEntirePortrait());

        // loots
        Map<LootType, Integer> loots = LootUtility.getLootValueMap(player);

        purses.setText(loots.get(LootType.PURSE).toString());

        diamonds.setText(loots.get(LootType.JEWEL).toString());

        suitcases.setText(loots.get(LootType.STRONGBOX).toString());

        int prize = 0;
        if (isBestShooter) {
            shooterPrize.setVisibility(VISIBLE);
            prize = 1000;
        }

        // total
        Integer tot = loots.get(LootType.PURSE) + loots.get(LootType.JEWEL) + loots.get(LootType.STRONGBOX)
                + prize;

        total.setText(tot.toString());
    }

    public void setWinner() {
        Drawable winnerFrame = getResources().getDrawable(R.drawable.winner);
        endPlayer.setBackground(winnerFrame);
    }
}