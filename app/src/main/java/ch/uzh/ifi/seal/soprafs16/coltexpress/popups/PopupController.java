package ch.uzh.ifi.seal.soprafs16.coltexpress.popups;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PossibilityDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Loot;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.LootType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.LootUtility;


/**
 * Handles the choose target overview.
 * Created by alex on 05.05.2016.
 */
public class PopupController implements ChooseTarget.OnChoiceListener,
        ChooseLoot.OnChoiceListener, ChoosePunchDirection.OnChoiceListener {

    private static final Logger logger = Logger.getLogger(PopupController.class.getSimpleName());

    private Context context;

    private View anchor;

    private FrameLayout parent;

    private PossibilityDTO possibilities;

    private List<IChoosePopup> popups;

    private Player chosenPlayer;

    private LootType chosenLootType;

    private boolean chosenDirection;

    private OnPossibilityChosenListener listener;

    public PopupController(View anchor, FrameLayout parent, Context context) {
        this.anchor = anchor;
        this.parent = parent;
        this.context = context;

        popups = new ArrayList<>();
    }

    /**
     * Sets possibilities for an action.
     *
     * @param possibilities Possibilities.
     */
    public void setPossibilities(PossibilityDTO possibilities) {
        this.possibilities = possibilities;
    }

    /**
     * Prepares and shows order of popups depending on played card.
     */
    public void show() {

        popups = new PopupFactory(context, parent, anchor).createPopups(possibilities.getType());

        for (IChoosePopup pop : popups) {
            pop.addOnChoiceListener(this);
            pop.setPossibilities(possibilities);
        }

        popups.remove(0).show();
    }

    /**
     * Builds a possibilityDTO with player choice.
     *
     * @return possibility transfer object to be sent to the server.
     */
    private PossibilityDTO buildChosenPossibility() {
        PossibilityDTO.Builder possibilityBuilder = possibilities.toBuilder();

        // Player chose a target
        if (chosenPlayer != null) {
            // Set chosen Player target
            List<Player> playerChosenList = new ArrayList<>();
            playerChosenList.add(chosenPlayer);

            possibilityBuilder.players(playerChosenList);
        }

        // Player chose a loot
        if (chosenLootType != null) {
            if (chosenPlayer != null) {
                // This is a punch card, not a loot card. Take only loots from player.
                possibilities = possibilities.toBuilder().loots(chosenPlayer.getLoots()).build();
            }

            List<Loot> lootsFromPossibilities = LootUtility.filterLootsByType(possibilities.getLoots(), chosenLootType);

            if (!lootsFromPossibilities.isEmpty()) {
                // Shuffle all possible loots
                Collections.shuffle(lootsFromPossibilities);
                Loot chosenLoot = lootsFromPossibilities.get(0);

                // Set chosen loot in possibilityDTO
                possibilityBuilder.lootId(chosenLoot.getId());
            }
        }

        possibilityBuilder.punchRight(chosenDirection);

        return possibilityBuilder.build();
    }

    public void addOnPossibilityChosenListener(OnPossibilityChosenListener listener) {
        this.listener = listener;
    }

    @Override
    public void onTargetChosen(Player player) {
        logger.severe("Player chose target player: " + player.toString());
        chosenPlayer = player;

        // Do it now to pass correct player to choose punch direction
        List<Player> players = new ArrayList<>();
        players.add(chosenPlayer);
        List<Loot> loots = chosenPlayer.getLoots();

        // If target has no loot, goto punch direction popup.
        if (!popups.isEmpty() && loots.isEmpty()) {
            popups.remove(0);
        }

        PossibilityDTO possibility = possibilities.toBuilder().players(players).loots(loots).build();
        for (IChoosePopup pop : popups) {
            pop.setPossibilities(possibility);
        }

        checkIfDoneChoosing();
    }

    @Override
    public void onLootChosen(LootType lootType) {
        logger.severe("Player chose target loot: " + lootType);
        chosenLootType = lootType;

        checkIfDoneChoosing();
    }

    @Override
    public void onPunchChosen(boolean punchRight) {
        logger.severe("Player chose direction: " + (punchRight ? "right" : "left"));
        chosenDirection = punchRight;

        checkIfDoneChoosing();
    }

    /**
     * Checks if player is done choosing, else continue to next choice.
     */
    private void checkIfDoneChoosing() {
        if (popups.isEmpty()) {
            // If done, notify gameActivity
            firePossibilityChosen();
        } else {
            // Else continue to next choice
            popups.remove(0).show();
        }
    }

    /**
     * Notifies game activity of player choice.
     */
    private void firePossibilityChosen() {
        if (listener != null) {
            PossibilityDTO chosenPossibility = buildChosenPossibility();
            listener.onPossibilityChosen(chosenPossibility);
        }
    }

    private class PopupFactory {

        private View anchor;
        private FrameLayout parent;
        private Context context;

        public PopupFactory(Context context, FrameLayout parent, View anchor) {
            this.context = context;
            this.parent = parent;
            this.anchor = anchor;
        }

        private List<IChoosePopup> createPopups(CardType type) {
            List<IChoosePopup> result = new ArrayList<>();
            switch (type) {
                case PUNCH:
                    result.add(createTargetPopup());
                    result.add(createLootPopup());
                    result.add(createPunchDirectionPopup());
                    break;
                case ROBBERY:
                    result.add(createLootPopup());
                    break;
                case FIRE:
                    result.add(createTargetPopup());
                    break;
            }

            return result;
        }

        private IChoosePopup createTargetPopup() {
            return new ChooseTarget(anchor, parent, context);
        }

        private IChoosePopup createLootPopup() {
            return new ChooseLoot(anchor, parent, context);
        }

        private IChoosePopup createPunchDirectionPopup() {
            return new ChoosePunchDirection(anchor, parent, context);
        }
    }
}