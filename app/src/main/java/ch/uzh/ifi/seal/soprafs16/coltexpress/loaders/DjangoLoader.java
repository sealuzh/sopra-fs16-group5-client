package ch.uzh.ifi.seal.soprafs16.coltexpress.loaders;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.HashMap;
import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;

@SuppressWarnings("deprecation")
public class DjangoLoader extends ImageLoaderFactory implements ImageLoader {

    public DjangoLoader(Context context) {
        setContext(context);
    }

    @Override
    public Map<CardType, Drawable> getCards() {
        Map<CardType, Drawable> cards = new HashMap<>();
        cards.put(CardType.PUNCH, context().getResources().getDrawable(R.drawable.django_punch_card));
        cards.put(CardType.ROBBERY, context().getResources().getDrawable(R.drawable.django_collect_card));
        cards.put(CardType.FIRE, context().getResources().getDrawable(R.drawable.django_shoot_card));
        cards.put(CardType.MOVE, context().getResources().getDrawable(R.drawable.django_moveh_card));
        cards.put(CardType.FLOORCHANGE, context().getResources().getDrawable(R.drawable.django_movev_card));
        cards.put(CardType.MARSHAL, context().getResources().getDrawable(R.drawable.django_sherif_card));
        cards.put(CardType.BULLET, context().getResources().getDrawable(R.drawable.neutral_bullet_card));
        return cards;
    }

    @Override
    public Drawable getSpecialAbility() {
        return context().getResources().getDrawable(R.drawable.django_abil);
    }

    @Override
    public Drawable getHalfPortrait() {
        return decodeSampledBitmapFromResource(context().getResources(), R.drawable.django, 340, 458);
    }

    @Override
    public Drawable getEntirePortrait() {
        return decodeSampledBitmapFromResource(context().getResources(), R.drawable.django_entire, 226, 489);
    }

    @Override
    public Drawable getMeeple() {
        return context().getResources().getDrawable(R.drawable.dj_meeple);
    }

    @Override
    public Drawable getCharacterCard() {
        return context().getResources().getDrawable(R.drawable.django_abili);
    }
}