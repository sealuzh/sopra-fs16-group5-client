package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.GameStatus;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoaderFactory;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Card;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Game;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Round;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.GameUtility;
import it.sephiroth.android.library.tooltip.Tooltip;

/**
 * Handles top center layout, composed of: global deck, round card and text
 * Created by a-a-hofmann on 19/04/16.
 */
public class TopCenterLayout extends LinearLayout {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(TopCenterLayout.class.getSimpleName());

    private CardView deck;
    private RoundCardLayout round;
    private TextView nrRound;
    private TextView phaseText;
    private Map<CardType, String> cardDescriptionMap;

    public TopCenterLayout(Context context) {
        super(context);
        initializeViews(context);
    }

    public TopCenterLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public TopCenterLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.top_center_layout, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        deck = (CardView) findViewById(R.id.globalDeckCard);
        round = (RoundCardLayout) findViewById(R.id.roundCard);
        //turn highlight here

        nrRound = (TextView) findViewById(R.id.nrRound);
        phaseText = (TextView) findViewById(R.id.phaseText);

        cardDescriptionMap = new HashMap<>();
        cardDescriptionMap.put(CardType.FIRE, "Choose one of your opponents to be your target and" +
                " give him one of your Bullet cards. The targeted player places the Bullet card" +
                " he has received on his deck. You cannot target a Bandit who is on the same spot " +
                "as you.\n" +
                "1 When you are inside the train, you can shoot a Bandit who is in the interior of an" +
                " adjacent Car, either to the rear of your Car or ahead of it. Bandits who are more " +
                "than one Car away cannot be shot.\n" +
                "2 When you are on the roof, however, you can shoot a Bandit who is in your Line of " +
                "Sight and on the roof of any Car other than your own, regardless of the distance. " +
                "A Bandit is in Line of Sight if there is no other Bandit between you and him. Two " +
                "Bandits on the roof of the same Car are considered as being side by side, and not " +
                "located one behind the other: you choose which one gets hit.\n" +
                "If there is no target to be shot, keep your Bullet card: the Fire action will have " +
                "no effect. If you run out of Bullet cards during the game, your Fire actions will have" +
                " no further effect.");
        cardDescriptionMap.put(CardType.FLOORCHANGE, "Switch the position of your Bandit from\n" +
                "1 TheinteriortotheroofoftheCar " +
                "on which he's standing;\n" +
                "2 or move his from the roof of the " +
                "Car he's in to its interior.");
        cardDescriptionMap.put(CardType.MARSHAL, "Move the Marshal inside the train a distance" +
                " of one Car in the direction of your choice. When a Bandit enters a Car where " +
                "the Marshal is, or when the Marshal enters a Car where Bandits are, they must " +
                "escape up to the roof of the Car (even if they have just come down from there). " +
                "A Bandit can never stay inside the Car where the Marshal is located.");
        cardDescriptionMap.put(CardType.MOVE, "Move your Bandit:\n" +
                "1 from one Car to the next adjacent Car, forwards\n" +
                "or backwards, if he is inside the Train; or\n" +
                "2 A distance ranging from one to three Cars (your " +
                "choice), forwards or backwards, if he is on the roof. The Locomotive is considered" +
                " to be a Car. You can be in it or on it. " +
                "If you have plotted this Action, your " +
                "Bandit cannot stay where he is: you must move him.");
        cardDescriptionMap.put(CardType.PUNCH, "Choose a target among the Bandits who are on the same " +
                "Car and same floor as you are. The targeted Bandit loses a Loot token if he has one: " +
                "choose a Loot token from his Character card and place it on the floor where your Bandit " +
                "pawn is located. If you choose a Purse token, you are not allowed to look at its value. " +
                "Then, move the targeted Bandit to the same floor of an adjacent Car (either forwards or " +
                "backwards, your choice in most of the Cars; backwards if you are in the Engine and forwards " +
                "if you are in the Caboose).");
        cardDescriptionMap.put(CardType.ROBBERY, "Take the Loot token of your choice from the Car where" +
                " you are currently located and place it face-down on your Character card. If your " +
                "Bandit is on the roof of a Car, he cannot rob inside it, and vice versa. If there is " +
                "no Loot where your Bandit is, then the Robbery action has no effect.");
    }

    public void setRounds(Long rounds, GameStatus status) {
        nrRound.setText(String.format("Round %s", rounds.toString()));

        if (GameStatus.PLANNINGPHASE.equals(status)) {
            phaseText.setText(getResources().getString(R.string.planning_phase));
        } else {
            phaseText.setText(getResources().getString(R.string.action_phase));
        }
    }

    public void setDefaultTopDeckCard() {
        deck.setImageDrawable(ImageLoaderFactory.getCardBackside(getContext()));
    }

    /**
     * Updates last played card.
     *
     * @param character Character that played last card.
     */
    private void setTopDeckCard(Character character, final CardType type, boolean isFaceDown) {
        if (!isFaceDown) {
            Drawable cardDrawable = ImageLoaderFactory.createLoader(getContext(), character).getCards().get(type);
            cardDrawable = cardDrawable.mutate();
            deck.setImageDrawable(cardDrawable);

            // Tooltip
            deck.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showCardDescription(type);
                }
            });

        } else {
            deck.setOnClickListener(null);
            deck.setImageDrawable(ImageLoaderFactory.getCardBackside(getContext()));
        }

        // Just to be sure
        deck.clearColorFilter();
        deck.setEnabled(true);
    }

    public void setTopDeckCard(Card card, GameUtility gameUtil) {
        Player cardOwner = gameUtil.findById(card.getOwnerId());
        if (!CardType.DRAW.equals(card.getType()) && cardOwner != null) {
            setTopDeckCard(cardOwner.getCharacter(), card.getType(),
                    card.getFaceDown() && !gameUtil.isActionPhase());
        }
    }

    public void setTopDeckCardInActionPhase(CardType type, Character character) {
        if (!CardType.DRAW.equals(type)) {
            setTopDeckCard(character, type, false);
        }
    }

    @SuppressWarnings("deprecation")
    public void setRound(Round currentRound) {
        round.setCurrentRound(currentRound);
    }

    public void displayCurrentTurn(Game game) {
        Long turn = game.getTurnId() - 1;
        round.displayCurrentTurn(turn);
    }

    public void showCardDescription(CardType lastPlayedCard) {
        Tooltip.make(getContext(),
                new Tooltip.Builder(101)
                        .anchor(deck, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 10000)
                        .activateDelay(800)
                        .showDelay(300)
                        .text(cardDescriptionMap.get(lastPlayedCard))
                        .maxWidth(500)
                        .withArrow(true)
                        .withOverlay(false)
                        .withStyleId(R.style.ToolTipLayoutCustomStyle)
                        .floatingAnimation(Tooltip.AnimationBuilder.SLOW)
                        .build()
        ).show();
    }
}