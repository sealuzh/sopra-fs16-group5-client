package ch.uzh.ifi.seal.soprafs16.coltexpress.service;

import android.content.Context;
import android.content.pm.ApplicationInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ch.uzh.ifi.seal.soprafs16.coltexpress.models.gson.AutoValueAdapterFactory;
import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class RestService {

    private static final String BASE_URL = "https://sopra-fs16-group5.herokuapp.com";

    private PlayersApiInterface playersApiInterface;
    private GamesApiInterface gamesApiInterface;
    private RoundsApiInterface roundsApiInterface;
    private static RestService instance;

    private RestService(Context context, Client client) {

        Gson gson = new GsonBuilder().registerTypeAdapterFactory(new AutoValueAdapterFactory()).create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setClient(client)
                .setConverter(new GsonConverter(gson))
                .build();

        boolean isDebuggable = 0 != (context.getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE);

        if (isDebuggable) {
            restAdapter.setLogLevel(RestAdapter.LogLevel.BASIC);
        }

        gamesApiInterface = restAdapter.create(GamesApiInterface.class);
        playersApiInterface = restAdapter.create(PlayersApiInterface.class);
        roundsApiInterface = restAdapter.create(RoundsApiInterface.class);
    }

    public static GamesApiInterface getGamesApiInstance(Context context) {
        if (instance == null) {
            instance = new RestService(context, new OkClient());
        }

        return instance.gamesApiInterface;
    }

    public static PlayersApiInterface getPlayersApiInstance(Context context) {
        if (instance == null) {
            instance = new RestService(context, new OkClient());
        }

        return instance.playersApiInterface;
    }

    public static RoundsApiInterface getRoundsApiInstance(Context context) {
        if (instance == null) {
            instance = new RestService(context, new OkClient());
        }

        return instance.roundsApiInterface;
    }
}
