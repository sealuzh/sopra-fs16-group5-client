package ch.uzh.ifi.seal.soprafs16.coltexpress.popups;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PossibilityDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.PunchDirectionLayout;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.GameUtility;

/**
 * Handles the choose target overview.
 * Created by alex on 05.05.2016.
 */
public class ChoosePunchDirection implements IChoosePopup {

    public interface OnChoiceListener extends ChoiceListener {
        void onPunchChosen(boolean punchRight);
    }

    private View anchor;

    private FrameLayout parent;

    private Context context;

    private PopupWindow popupWindow;

    private LinearLayout container;

    private PossibilityDTO possibilities;

    private OnChoiceListener listener;

    private View punchView;

    private boolean chosenDirection;

    public ChoosePunchDirection(View anchor, FrameLayout parent, Context context) {
        this.anchor = anchor;
        this.parent = parent;
        this.context = context;

        init();
    }

    /**
     * Initialize Popup.
     */
    private void init() {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        punchView = layoutInflater.inflate(R.layout.punch_direction_popup, null);
        popupWindow = new PopupWindow(punchView, 500, 150);
        container = (LinearLayout) punchView.findViewById(R.id.parent);
    }

    /**
     * Dismisses current Popup.
     */
    private void dismiss() {
        popupWindow.dismiss();
        parent.getForeground().setAlpha(0);
    }

    @Override
    public void setPossibilities(PossibilityDTO possibilities) {
        this.possibilities = possibilities;
    }

    /**
     * Shows view to choose a player target.
     */
    @Override
    public void show() {
        punchView.getBackground().setAlpha(150);
        popupWindow.showAsDropDown(anchor, -80, 200);

        // Drop existing opponents overview
        container.removeAllViews();

        final PunchDirectionLayout l = new PunchDirectionLayout(punchView.getContext(), isRightActive(), isLeftActive());

        l.getChooseButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chosenDirection = l.isRight();

                if (listener != null) {

                    // Send chosen possibility back to the server
                    listener.onPunchChosen(chosenDirection);

                    dismiss();
                }
            }
        });

        container.addView(l);
    }

    /**
     * Checks if right arrow should be active.
     *
     * @return {@code true} iff player can move right.
     */
    private boolean isRightActive() {
        Player target = possibilities.getPlayers().get(0);

        return target.getCar() != 0;

    }

    /**
     * Checks if left arrow should be active.
     *
     * @return {@code true} iff player can move left.
     */
    private boolean isLeftActive() {
        Player target = possibilities.getPlayers().get(0);

        return target.getCar() != GameUtility.nrOfCars - 1;
    }

    @Override
    public void addOnChoiceListener(ChoiceListener listener) {
        this.listener = (OnChoiceListener) listener;
    }
}