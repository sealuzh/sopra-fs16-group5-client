package ch.uzh.ifi.seal.soprafs16.coltexpress.service;

import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.CardTypeDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PossibilityDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Round;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by a-a-hofmann on 29/04/16.
 */
public interface RoundsApiInterface {

    @POST("/games/{gameId}/rounds/{roundId}/turns")
    void playCard(@Path("gameId") Long gameId, @Path("roundId") Long roundId,
                  @Query("token") String token, @Body CardTypeDTO type, Callback<String> cb);

    @GET("/games/{gameId}/rounds")
    void getRound(@Path("gameId") Long gameId, Callback<Round> cb);

    @POST("/games/{gameId}/actions")
    void getPossibilities(@Path("gameId") Long gameId, @Query("token") String token, Callback<PossibilityDTO> cb);

    @PUT("/games/{gameId}/actions")
    void playPossibility(@Path("gameId") Long gameId, @Query("token") String token, @Body PossibilityDTO possibility, Callback<Void> cb);
}
