package ch.uzh.ifi.seal.soprafs16.coltexpress.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.GameStatus;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Game;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.GameJoinService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.GameService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.PlayerService;
import ch.uzh.ifi.seal.soprafs16.coltexpress.service.ReplaceFont;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.GameUtility;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.MusicPlayer;

/**
 * The {@code CharactersActivity} class handles a lobby for a game.
 * Created by francesca on 23.03.16.
 */
public class CharactersActivity extends Activity implements GameService.GameStartListener,
        GameService.GameReceivedListener {

    private static final Logger logger = Logger.getLogger(CharactersActivity.class.getName());

    private Button startGameButton;

    private Button quickGameButton;

    private Character character;

    private GameService gameService;

    private PlayerService playerService;

    private GameJoinService gameJoinService;

    private Map<Integer, Character> characterMap;

    private PlayerListRefresher playerListRefresher;

    private MusicPlayer musicPlayer;

    @Override
    protected void onPause() {
        super.onPause();
        playerListRefresher.cancel(true);
        playerListRefresher = null;

        musicPlayer.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        playerListRefresher = new PlayerListRefresher();
        playerListRefresher.execute();

        musicPlayer.playOrResume(R.raw.silent_film);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ReplaceFont.replaceDefaultFont(this, "DEFAULT", "CFOldMilwaukee-Regular.ttf");
        setContentView(R.layout.activity_characters);

        musicPlayer = MusicPlayer.getInstance(this);

        initializeService();
        createCharacterMap();

        gameService.setOnGameStartedListener(this);

        initButtons(playerService.getPlayer(), gameService.getGame());
    }

    /**
     * Initializes and sets click listeners on quick game and start game buttons.
     *
     * @param player User's player to check if he is the owner of the game.
     *               Only the owner of a game is allowed to start it.
     * @param game   Game to be started.
     */
    private void initButtons(final Player player, final Game game) {
        startGameButton = (Button) findViewById(R.id.start_game);
        quickGameButton = (Button) findViewById(R.id.quick_game);

        if (!player.getUsername().equals(game.getOwner())) {
            startGameButton.setEnabled(false);
            startGameButton.setVisibility(View.INVISIBLE);
            quickGameButton.setEnabled(false);
            quickGameButton.setVisibility(View.INVISIBLE);
        }

        startGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playerService.getPlayer().getCharacter() == null) {
                    startGameButton.setError(getString(R.string.game_start_error));
                } else {
                    showProgressDialog();
                    gameService.startGame(game.getId(), player.getToken());
                }
            }
        });

        quickGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playerService.getPlayer().getCharacter() == null) {
                    quickGameButton.setError(getString(R.string.game_start_error));
                } else {
                    showProgressDialog();
                    gameService.startQuickGame(game.getId(), player.getToken());
                }
            }
        });
    }

    private void showProgressDialog() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("We're preparing a game for you, please wait.");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void createCharacterMap() {
        characterMap = new HashMap<>();
        characterMap.put(R.id.button_b, Character.BELLE);
        characterMap.put(R.id.button_c, Character.CHEYENNE);
        characterMap.put(R.id.button_d, Character.DJANGO);
        characterMap.put(R.id.button_doc, Character.DOC);
        characterMap.put(R.id.button_g, Character.GHOST);
        characterMap.put(R.id.button_t, Character.TUCO);
    }

    /**
     * Initialize service objects.
     */
    private void initializeService() {
        playerService = PlayerService.getInstance(this);
        gameService = GameService.getInstance(this);
        gameJoinService = GameJoinService.getInstance(this);
    }

    /**
     * Joins a game with character chosen from button.
     *
     * @param view Button that was clicked.
     */
    public void chooseCharacter(View view) {
        character = characterMap.get(view.getId());
        view.setEnabled(false);
        logger.info("User " + playerService.getPlayer().getUsername() + " chose: " + character);
        joinGame();

        // Display ability text of clicked character
        TextView abilDescription = (TextView) findViewById(R.id.ability_desc);

        if (character == Character.BELLE) {
            abilDescription.setText(R.string.belle_ability_desc);
        } else if (character == Character.CHEYENNE) {
            abilDescription.setText(R.string.cheyenne_ability_desc);
        } else if (character == Character.DJANGO) {
            abilDescription.setText(R.string.django_ability_desc);
        } else if (character == Character.DOC) {
            abilDescription.setText(R.string.doc_ability_desc);
        } else if (character == Character.GHOST) {
            abilDescription.setText(R.string.ghost_ability_desc);
        } else if (character == Character.TUCO) {
            abilDescription.setText(R.string.tuco_ability_desc);
        }
        abilDescription.setVisibility(View.VISIBLE);
    }

    /**
     * Joins a game.
     * It delegates to GameService.
     */
    private void joinGame() {
        Player player = playerService.getPlayer();
        Game game = gameService.getGame();

        if (game != null && player != null
                && game.getOwner() != null && player.getUsername() != null) {

            // if the player has no character then use chooseCharacter() to choose a character
            if (game.getOwner().equals(player.getUsername()) || null != player.getCharacter()) {
                gameJoinService.chooseCharacter(game.getId(), player, character);
            } else {
                // else use joinGame()
                gameJoinService.joinGame(game.getId(), player, character);
            }
        } else {
            logger.severe("Player or game (or both) are null");
        }
    }

    @Override
    public void onGameStarted() {
        // Stop waiting on updates for game start.
        gameService.removeOnGameStartedListener();

        // Fetch initialized game
        gameService.setOnGameReceivedListener(this);
        gameService.getGameFromNetwork(gameService.getGame().getId());
    }

    @Override
    public void onGameReceived(Game result) {
        // Game setup
        gameService.setGame(result);

        // Update user infos
        GameUtility gameUtil = new GameUtility(result, playerService.getPlayer().getId());
        playerService.setPlayer(gameUtil.getPlayer());

        //Start game once we receive the correctly started game from server
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    /**
     * The class {@code PlayerListRefresher} handles the network calls to refresh the list of
     * available character.
     */
    public class PlayerListRefresher extends AsyncTask<Void, List<Player>, Void>
            implements GameService.GameReceivedListener {

        /**
         * Character buttons in this view.
         */
        private Map<Character, Button> buttons;

        private static final String FROM1 = "username";
        private static final String FROM2 = "character";

        /**
         * Mapping from items in Map.
         */
        private final String[] from = {FROM1, FROM2};

        /**
         * Map to items in ListView.
         */
        private final int[] to = {android.R.id.text1, android.R.id.text2};

        /**
         * Listview to display list of players.
         */
        private ListView listView;

        private SimpleAdapter adapter;

        private List<Map<String, String>> playerList;

        private ColorMatrixColorFilter bwFilter;

        /**
         * Called from the UI thread on .execute() to setup the task.
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);
            bwFilter = new ColorMatrixColorFilter(matrix);

            // prepare UI
            initializeButtons();
            disableAllButtons();

            playerList = new ArrayList<>();

            listView = (ListView) findViewById(R.id.listPlayerView);

            adapter = new SimpleAdapter(CharactersActivity.this, playerList, android.R.layout.simple_list_item_2, from, to);

            listView.setAdapter(adapter);

            // Listen for new games from gameService
            gameService.setOnGameReceivedListener(this);
        }

        /**
         * Creates a map to easily access character buttons.
         */
        private void initializeButtons() {
            buttons = new HashMap<>();
            buttons.put(Character.BELLE, (Button) findViewById(R.id.button_b));
            buttons.put(Character.CHEYENNE, (Button) findViewById(R.id.button_c));
            buttons.put(Character.DJANGO, (Button) findViewById(R.id.button_d));
            buttons.put(Character.DOC, (Button) findViewById(R.id.button_doc));
            buttons.put(Character.GHOST, (Button) findViewById(R.id.button_g));
            buttons.put(Character.TUCO, (Button) findViewById(R.id.button_t));
        }

        /**
         * Disables all buttons.
         */
        private void disableAllButtons() {

            for (Map.Entry<Character, Button> entry : buttons.entrySet()) {
                entry.getValue().getBackground().setColorFilter(bwFilter);
                entry.getValue().setEnabled(false);
            }
        }

        /**
         * Every time {@code publishProgress()} is called, onProgressUpdate is called on the UI
         * thread updating the list of characters.
         *
         * @param values Characters from the server.
         */
        @Override
        protected void onProgressUpdate(List<Player>... values) {
            super.onProgressUpdate(values);
            List<Player> players = values[0];
            showAvailable(players);
            showListOfPlayers(players);

            Drawable startGameButtonImage = startGameButton.getBackground();
            Drawable quickGameButtonImage = quickGameButton.getBackground();
            if (players.size() > 1 && haveAllPlayersChosen(players)) {
                startGameButton.setEnabled(true);
                startGameButton.setError(null);
                quickGameButton.setEnabled(true);
                quickGameButton.setError(null);

                if (startGameButtonImage !=  null && quickGameButtonImage != null) {
                    startGameButtonImage.clearColorFilter();
                    quickGameButtonImage.clearColorFilter();
                }
            } else {
                startGameButton.setEnabled(false);
                quickGameButton.setEnabled(false);
                if (startGameButtonImage !=  null && quickGameButtonImage != null) {
                    startGameButtonImage.setColorFilter(bwFilter);
                    quickGameButtonImage.setColorFilter(bwFilter);
                }
            }
        }

        /**
         * Checks if all players have chosen a character to begin game.
         *
         * @param players Players in this lobby.
         * @return {@code true} iff all players have chosen a character, {@code false} otherwise.
         */
        private boolean haveAllPlayersChosen(List<Player> players) {
            for (Player p : players) {
                if (p.getCharacter() == null) {
                    return false;
                }
            }

            return true;
        }

        /**
         * Runs in background off of the UI thread to avoid blocking screen activity.
         * Sends GETs to the /games/{gameId} service to update list of available characters
         * and players in the lobby.
         *
         * @param params UI Context
         * @return nothing
         */
        @Override
        protected Void doInBackground(Void... params) {
            Game game = gameService.getGame();
            Long gameId = game.getId();

            while (true) {
                gameService.getGameFromNetwork(gameId);

                if (isCancelled()) {
                    break;
                }

                SystemClock.sleep(500);
            }

            return null;
        }

        /**
         * Updates ListView with players received from the server.
         *
         * @param players List of players
         */
        private void showListOfPlayers(List<Player> players) {

            for (Player p : players) {
                if (!isInPlayerList(p)) {
                    playerList.add(createListItem(p));
                }
            }

            adapter.notifyDataSetChanged();
        }

        /**
         * Check if player is already in lobby list.
         *
         * @param player Player to check if in list.
         * @return {@code true} iff player is already in list. {@code false} otherwise.
         */
        private boolean isInPlayerList(Player player) {
            for (Map<String, String> playerMap : playerList) {
                String username = playerMap.get(FROM1);
                if (username.equals(player.getUsername())) {
                    if (player.getCharacter() != null) {
                        playerMap.put(FROM2, player.getCharacter().toString());
                    }
                    return true;
                }
            }

            return false;
        }

        /**
         * Creates a Map from a player.
         *
         * @param player Player to be added to the list.
         * @return Map containing username and character.
         */
        private Map<String, String> createListItem(Player player) {
            Map<String, String> item = new HashMap<>();

            String username = player.getUsername();
            Character c = player.getCharacter();

            item.put(FROM1, username);
            if (c != null) {
                item.put(FROM2, c.toString());
            } else {
                item.put(FROM2, "");
            }

            return item;
        }

        /**
         * Enables button to demo available characters
         *
         * @param players List of players in this game.
         */
        private void showAvailable(List<Player> players) {
            disableAllButtons();

            List<Character> availableCharacters = new ArrayList<>(Arrays.asList(Character.values()));
            for (Player p : players) {
                availableCharacters.remove(p.getCharacter());
            }

            // Remove Marshal
            availableCharacters.remove(Character.MARSHAL);

            // Re-enable only the ones that are available
            for (Character c : availableCharacters) {
                buttons.get(c).getBackground().clearColorFilter();
                buttons.get(c).setEnabled(true);
            }
        }

        @Override
        public void onGameReceived(Game result) {
            List<Player> players = result.getPlayers();
            onProgressUpdate(players);

            // If the game started then move to gameActivity
            GameStatus status = result.getStatus();
            if (status != null && !status.equals(GameStatus.PENDING)) {
                gameService.removeOnGameReceivedListener();
                gameService.fireGameStarted();
            }
        }
    }
}
