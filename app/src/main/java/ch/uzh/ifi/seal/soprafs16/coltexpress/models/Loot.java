package ch.uzh.ifi.seal.soprafs16.coltexpress.models;

import android.os.Parcelable;

import auto.parcel.AutoParcel;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.gson.AutoGson;

/**
 * Created by a-a-hofmann on 19/04/16.
 */
@AutoParcel
@AutoGson
public abstract class Loot implements Parcelable {

    public abstract Long getId();

    public abstract LootType getType();

    public abstract Integer getValue();

    public abstract Integer getCar();

    public abstract Level getLevel();

    public static Loot create(Long id,
                              LootType type,
                              Integer value,
                              Integer car,
                              Level level) {
        return builder()
                .id(id)
                .type(type)
                .value(value)
                .car(car)
                .level(level)
                .build();
    }

    public static Builder builder() {
        return new AutoParcel_Loot.Builder();
    }

    @AutoParcel.Builder
    public interface Builder {
        Builder id(Long id);

        Builder type(LootType type);

        Builder value(Integer value);

        Builder car(Integer car);

        Builder level(Level level);

        Loot build();
    }
}
