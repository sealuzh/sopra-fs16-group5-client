package ch.uzh.ifi.seal.soprafs16.coltexpress.popups;

import android.app.ActionBar;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import java.util.List;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PossibilityDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.PortraitOverviewLayout;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;

/**
 * Handles the choose target overview.
 * Created by alex on 05.05.2016.
 */
public class ChooseTarget implements IChoosePopup {

    public interface OnChoiceListener extends ChoiceListener {
        void onTargetChosen(Player player);
    }

    private View anchor;

    private FrameLayout parent;

    private Context context;

    private PopupWindow popupWindow;

    private LinearLayout container;

    private OnChoiceListener listener;

    private Player chosenTarget;

    private PossibilityDTO possibilities;

    private View punchFireView;

    private Button choose;

    public ChooseTarget(View anchor, FrameLayout parent, Context context) {
        this.anchor = anchor;
        this.parent = parent;
        this.context = context;

        init();
    }

    /**
     * Initialize Popup.
     */
    private void init() {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        punchFireView = layoutInflater.inflate(R.layout.target_popup, null);

        popupWindow = new PopupWindow(
                punchFireView,
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);

        choose = (Button) punchFireView.findViewById(R.id.choose);
        choose.setEnabled(false);
        choose.setVisibility(View.INVISIBLE);

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null && chosenTarget != null) {

                    // Send chosen possibility back to the server
                    listener.onTargetChosen(chosenTarget);

                    dismiss();
                }
            }
        });
        container = (LinearLayout) punchFireView.findViewById(R.id.parent);
    }

    /**
     * Dismisses current Popup.
     */
    private void dismiss() {
        popupWindow.dismiss();
        parent.getForeground().setAlpha(0);
    }

    public void setPossibilities(PossibilityDTO possibilities) {
        this.possibilities = possibilities;
    }

    /**
     * Shows view to choose a player target.
     */
    public void show() {
        punchFireView.getBackground().setAlpha(150);
        popupWindow.showAsDropDown(anchor, -80, -100);

        // Drop existing opponents overview
        container.removeAllViews();

        List<Player> opponents = possibilities.getPlayers();
        // Compute weight
        int nrOpp = opponents.size();
        float weight = 1.0f / nrOpp;

        for (Player p : opponents) {
            // Create an overview layout
            PortraitOverviewLayout l = new PortraitOverviewLayout(punchFireView.getContext());

            // Set player info
            l.setInfo(p);

            // Set clicklistener
            l.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disableLayouts();
                    PortraitOverviewLayout l = (PortraitOverviewLayout) v;
                    l.setEnabled(true);

                    choose.setEnabled(true);
                    choose.setVisibility(View.VISIBLE);

                    chosenTarget = l.getTarget();
                }
            });

            // set weight
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) l.getLayoutParams();
            if (params != null) {
                params.weight = weight;
            } else {
                params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, weight);
            }
            l.setLayoutParams(params);

            container.addView(l);
        }
    }

    /**
     * Disables all layouts.
     */
    private void disableLayouts() {
        for (int i = 0; i < container.getChildCount(); i++) {
            container.getChildAt(i).setEnabled(false);
        }
    }

    @Override
    public void addOnChoiceListener(ChoiceListener listener) {
        this.listener = (OnChoiceListener) listener;
    }
}
