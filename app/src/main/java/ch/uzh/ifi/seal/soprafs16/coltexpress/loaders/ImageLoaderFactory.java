package ch.uzh.ifi.seal.soprafs16.coltexpress.loaders;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;

@SuppressWarnings("deprecation")
public abstract class ImageLoaderFactory {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(ImageLoaderFactory.class.getSimpleName());

    private static Map<Character, ImageLoader> characterLoaderMap;

    private static SparseArray<Drawable> bulletsArray;

    private Context context;

    public Context context() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private static void characterLoaderMapInit(Context context) {
        characterLoaderMap = new HashMap<>();
        characterLoaderMap.put(Character.BELLE, new BelleLoader(context));
        characterLoaderMap.put(Character.DOC, new DocLoader(context));
        characterLoaderMap.put(Character.CHEYENNE, new CheyenneLoader(context));
        characterLoaderMap.put(Character.DJANGO, new DjangoLoader(context));
        characterLoaderMap.put(Character.TUCO, new TucoLoader(context));
        characterLoaderMap.put(Character.GHOST, new GhostLoader(context));
        characterLoaderMap.put(Character.MARSHAL, new MarshalLoader(context));
    }

    private static void bulletMapInit(Context context) {
        bulletsArray = new SparseArray<>();
        bulletsArray.put(0, context.getResources().getDrawable(R.drawable.roundhighlight));
        bulletsArray.put(1, context.getResources().getDrawable(R.drawable.colt_shot_1));
        bulletsArray.put(2, context.getResources().getDrawable(R.drawable.colt_shot_2));
        bulletsArray.put(3, context.getResources().getDrawable(R.drawable.colt_shot_3));
        bulletsArray.put(4, context.getResources().getDrawable(R.drawable.colt_shot_4));
        bulletsArray.put(5, context.getResources().getDrawable(R.drawable.colt_shot_5));
        bulletsArray.put(6, context.getResources().getDrawable(R.drawable.colt_shot_6));
    }

    public static Drawable getMoveMeeple(Context context) {
        return context.getResources().getDrawable(R.drawable.grey_meeple);
    }

    public static Drawable getCardBackside(Context context) {
        return context.getResources().getDrawable(R.drawable.card_backside);
    }

    public static ImageLoader createLoader(Context context, Character character) {
        if (characterLoaderMap == null) {
            characterLoaderMapInit(context);
        }
        return characterLoaderMap.get(character);
    }

    public static Drawable getBulletPic(Context context, int bulletsLeft) {
        if (bulletsArray == null) {
            bulletMapInit(context);
        }
        return bulletsArray.get(bulletsLeft);
    }

    public static BitmapDrawable getRoundCard(Context context, String roundString) {
        final int roundCardWidth = 140;
        final int roundCardHeight = 100;

        Map<String, Integer> roundCardsMap = new HashMap<>();
        roundCardsMap.put("ndn", R.drawable.round_ndn);
        roundCardsMap.put("nhdn", R.drawable.round_nhdn);
        roundCardsMap.put("nhnhn", R.drawable.round_nhnhn);
        roundCardsMap.put("nhnn", R.drawable.round_nhnn);
        roundCardsMap.put("nnhnn", R.drawable.round_nnhnn);
        roundCardsMap.put("nnhr", R.drawable.round_nnhr);
        roundCardsMap.put("nnnn", R.drawable.round_nnnn);
        roundCardsMap.put("1", R.drawable.bhf_1); //Pickpocketing
        roundCardsMap.put("2", R.drawable.bhf_2); //Marshal
        roundCardsMap.put("3", R.drawable.bhf_3); //Hostage
        return decodeSampledBitmapFromResource(context.getResources(), roundCardsMap.get(roundString),
                roundCardWidth, roundCardHeight);
    }

    public static BitmapDrawable[] getRules(Context context) {
        final int rulesWidth = 1653;
        final int rulesHeight = 1653;
        BitmapDrawable[] rules = new BitmapDrawable[3];

        try {
            rules[0] = decodeSampledBitmapFromResource(context.getResources(), R.drawable.rules1,
                    rulesWidth, rulesHeight);
            rules[1] = decodeSampledBitmapFromResource(context.getResources(), R.drawable.rules2,
                    rulesWidth, rulesHeight);
            rules[2] = decodeSampledBitmapFromResource(context.getResources(), R.drawable.rules3,
                    rulesWidth, rulesHeight);
        } catch (OutOfMemoryError e) {
            logger.severe(e.getMessage());
            logger.severe("Failed to open instructions!");
        }

        return rules;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static BitmapDrawable decodeSampledBitmapFromResource(Resources res, int resId,
                                                                 int reqWidth, int reqHeight) throws OutOfMemoryError {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return new BitmapDrawable(BitmapFactory.decodeResource(res, resId, options));
    }
}