package ch.uzh.ifi.seal.soprafs16.coltexpress.service;

import android.content.Context;

import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.CardTypeDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PossibilityDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Round;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * The {@code RoundService} is used to play a card.
 * Created by a-a-hofmann on 29/04/16.
 */
public class RoundService {

    public interface OnCardPlayedListener {
        void onCardPlayed();
    }

    public interface OnRoundReceivedListener {
        void onRoundReceived(Round currentRound);
    }

    public interface OnPossibilitiesReceivedListener {
        void onPossibilitiesReceived(PossibilityDTO possibilities);
    }

    public interface OnPossibilityPlayedListener {
        void onPossibilityPlayed();
    }

    private static Logger logger = Logger.getLogger(RoundService.class.getSimpleName());

    private static RoundService roundService;

    private OnCardPlayedListener cardPlayedListener;

    private OnRoundReceivedListener roundReceivedListener;

    private OnPossibilitiesReceivedListener possibilitiesReceivedListener;

    private Context context;

    private RoundService() {
    }

    public static RoundService getInstance(Context context) {
        if (roundService == null) {
            roundService = new RoundService();
        }
        roundService.context = context;
        return roundService;
    }

    public void playCard(Long gameId, Long roundId, CardType type, String token) {
        CardTypeDTO dto = CardTypeDTO.create(type);

        RestService.getRoundsApiInstance(context).playCard(gameId, roundId, token, dto, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                if (cardPlayedListener != null) {
                    cardPlayedListener.onCardPlayed();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logger.severe(error.getUrl());
                logger.severe(error.getMessage());
            }
        });
    }

    public void getRound(Long gameId) {
        RestService.getRoundsApiInstance(context).getRound(gameId, new Callback<Round>() {
            @Override
            public void success(Round round, Response response) {
                if (roundReceivedListener != null) {
                    roundReceivedListener.onRoundReceived(round);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logger.severe(error.getUrl());
                logger.severe(error.getMessage());
            }
        });
    }

    public void drawCards(Long gameId, Long roundId, String token) {
        playCard(gameId, roundId, CardType.DRAW, token);
    }

    public void getPossibilities(Long gameId, String token) {
        RestService.getRoundsApiInstance(context).getPossibilities(gameId, token, new Callback<PossibilityDTO>() {
            @Override
            public void success(PossibilityDTO possibilityDTO, Response response) {
                if (possibilitiesReceivedListener != null) {
                    possibilitiesReceivedListener.onPossibilitiesReceived(possibilityDTO);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logger.severe(error.getUrl());
                logger.severe(error.getMessage());
            }
        });
    }

    public void playPossibility(Long gameId, String token, PossibilityDTO possibilityDTO, final OnPossibilityPlayedListener listener) {
        RestService.getRoundsApiInstance(context).playPossibility(gameId, token, possibilityDTO, new Callback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
                logger.info("Possibility played successfully");
                if (listener != null) {
                    listener.onPossibilityPlayed();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logger.severe("Failed to play possibility");
                logger.severe(error.getUrl());
                logger.severe(error.getMessage());
            }
        });
    }

    public void addOnCardPlayedListener(OnCardPlayedListener listener) {
        this.cardPlayedListener = listener;
    }

    public void addOnRoundReceivedListener(OnRoundReceivedListener listener) {
        this.roundReceivedListener = listener;
    }

    public void addOnPossibilitiesReceivedListener(OnPossibilitiesReceivedListener listener) {
        this.possibilitiesReceivedListener = listener;
    }

    public void removeOnCardPlayedListener() {
        cardPlayedListener = null;
    }

    public void removeOnRoundReceivedListener() {
        roundReceivedListener = null;
    }

    public void removeOnPossibilitiesReceivedListener() {
        possibilitiesReceivedListener = null;
    }
}