package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoaderFactory;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;


//only for meeple_Car_Layout yet
public class MeepleLayout extends LinearLayout implements IMeepleLayout {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MeepleLayout.class.getSimpleName());

    private List<ImageView> meeples;

    private List<Character> characters;

    private Animation animation;

    public MeepleLayout(Context context) {
        super(context);
        initializeViews(context);
    }

    public MeepleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public MeepleLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }


    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.meeple_car_layout, this);

        meeples = new ArrayList<>();
        characters = new ArrayList<>();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        meeples.add((ImageView) findViewById(R.id.meeple1));
        meeples.add((ImageView) findViewById(R.id.meeple2));
        meeples.add((ImageView) findViewById(R.id.meeple3));
        meeples.add((ImageView) findViewById(R.id.meeple4));

        animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
    }

    //update meeples
    public void setMeeples(List<Player> players) {
        for (ImageView v : meeples) {
            v.setVisibility(INVISIBLE);
        }

        int i = 0;
        for (Player p : players) {
            meeples.get(i).setVisibility(VISIBLE);
            meeples.get(i++).setImageDrawable(ImageLoaderFactory.createLoader(getContext(), p.getCharacter()).getMeeple());

            setInvisible();
        }
    }

    @Override
    public void addMeeple(Character character) {

        int currentMeeple = characters.size();

        if (currentMeeple < 4) {
            characters.add(character);
            meeples.get(currentMeeple).setImageDrawable(ImageLoaderFactory.createLoader(getContext(), character).getMeeple());
            meeples.get(currentMeeple).setVisibility(VISIBLE);
        }
    }

    public void addMoves() {
        int currentMeeple = characters.size();
        meeples.get(currentMeeple).setImageDrawable(getContext().getResources().getDrawable(R.drawable.grey_meeple));
        meeples.get(currentMeeple).setVisibility(VISIBLE);

        //blinking
        meeples.get(currentMeeple).startAnimation(animation);
    }

    @Override
    public void stopAnimation() {
        for (ImageView m : meeples) {
            m.clearAnimation();
        }
    }

    private void setInvisible() {
        for (ImageView v : meeples) {
            v.setVisibility(INVISIBLE);
        }
    }

    @Override
    public void emptyLayout() {
        characters = new ArrayList<>();
        for (ImageView v : meeples) {
            v.setImageDrawable(null);
        }
    }
}