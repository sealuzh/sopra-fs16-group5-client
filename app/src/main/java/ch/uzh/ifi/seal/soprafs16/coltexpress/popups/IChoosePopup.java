package ch.uzh.ifi.seal.soprafs16.coltexpress.popups;

import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PossibilityDTO;

/**
 * Created by a-a-hofmann on 11/05/16.
 */
public interface IChoosePopup {

    /**
     * Shows popup to player.
     */
    void show();

    /**
     * Sets possibilities for an action.
     *
     * @param possibilities Possibilities.
     */
    void setPossibilities(PossibilityDTO possibilities);

    void addOnChoiceListener(ChoiceListener listener);
}
