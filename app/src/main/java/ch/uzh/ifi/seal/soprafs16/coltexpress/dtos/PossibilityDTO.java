package ch.uzh.ifi.seal.soprafs16.coltexpress.dtos;

import android.support.annotation.Nullable;

import java.util.List;

import auto.parcel.AutoParcel;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.CardType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Loot;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.gson.AutoGson;

/**
 * Created by a-a-hofmann on 29/04/16.
 */
@AutoGson
@AutoParcel
public abstract class PossibilityDTO {

    public abstract CardType getType();

    public abstract List<Player> getPlayers();

    public abstract List<Loot> getLoots();

    public abstract List<Integer> getMarshal();

    public abstract boolean getPunchRight();

    @Nullable
    public abstract Long getLootId();

    public static PossibilityDTO create(CardType type, List<Player> players, List<Loot> loots,
                                        boolean punchRight, @Nullable Long lootId, List<Integer> marshal) {
        return builder().type(type).players(players).loots(loots).punchRight(punchRight)
                .lootId(lootId).marshal(marshal).build();
    }

    public static Builder builder() {
        return new AutoParcel_PossibilityDTO.Builder();
    }

    public abstract Builder toBuilder();

    @AutoParcel.Builder
    public interface Builder {
        Builder type(CardType type);

        Builder players(List<Player> players);

        Builder loots(List<Loot> loots);

        Builder punchRight(boolean punchRight);

        Builder lootId(Long lootId);

        Builder marshal(List<Integer> marshal);

        PossibilityDTO build();
    }
}
