package ch.uzh.ifi.seal.soprafs16.coltexpress.constants;

/**
 * All possible type of Roundcards.
 * Created by selina
 */
public enum RoundType {
    /*
     N normal
     H hidden
     D doubleTurn
     R reverse
     bhf 1 pickpocketing end event
     bhf 2 marshals revenge end event
     bhf 3 Hostage-Taking of the Conductor end event
     */
    NDN,
    NHDN,
    NHNHN,
    NHNN,
    NNHNN,
    NNHR,
    NNNN,
    PICKPOCKETING,
    MARSHAL,
    HOSTAGE,
    NONE
}