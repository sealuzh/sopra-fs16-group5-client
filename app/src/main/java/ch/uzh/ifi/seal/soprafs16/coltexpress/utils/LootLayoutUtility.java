package ch.uzh.ifi.seal.soprafs16.coltexpress.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.activities.GameActivity;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.LootLayout;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Level;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Loot;

/**
 * Helper class to manipulate loot layouts for the entire train.
 * Created by a-a-hofmann on 22/04/16.
 */
public class LootLayoutUtility {

    private final int nrOfCars;

    private final GameActivity activity;

    private List<Map<Level, LootLayout>> lootLayouts;


    public LootLayoutUtility(GameActivity activity, int nrOfCars) {
        this.activity = activity;
        this.nrOfCars = nrOfCars;
        lootLayouts = new ArrayList<>();

        initViews();
    }

    /**
     * Find all views and initialize the layouts.
     */
    private void initViews() {

        // loco + 2 cars are always present
        lootLayouts.add(createMap(
                (LootLayout) activity.findViewById(R.id.loot_car0),
                (LootLayout) activity.findViewById(R.id.loot_car0_top)));

        lootLayouts.add(createMap(
                (LootLayout) activity.findViewById(R.id.loot_car1),
                (LootLayout) activity.findViewById(R.id.loot_car1_top)));

        lootLayouts.add(createMap(
                (LootLayout) activity.findViewById(R.id.loot_car2),
                (LootLayout) activity.findViewById(R.id.loot_car2_top)));

        // At least 3 player game
        if (nrOfCars > 3) {
            lootLayouts.add(createMap(
                    (LootLayout) activity.findViewById(R.id.loot_car3),
                    (LootLayout) activity.findViewById(R.id.loot_car3_top)));

            // 4 player game
            if (nrOfCars > 4) {
                lootLayouts.add(createMap(
                        (LootLayout) activity.findViewById(R.id.loot_car4),
                        (LootLayout) activity.findViewById(R.id.loot_car4_top)));
            }
        }
    }

    private Map<Level, LootLayout> createMap(LootLayout bottom, LootLayout top) {
        Map<Level, LootLayout> lootMapItem = new HashMap<>();
        lootMapItem.put(Level.BOTTOM, bottom);
        lootMapItem.put(Level.TOP, top);

        return lootMapItem;
    }

    /**
     * Makes all loot layouts invisible.
     */
    private void invalidate() {
        for (Map<Level, LootLayout> map : lootLayouts) {
            map.get(Level.BOTTOM).emptyLayout();
            map.get(Level.TOP).emptyLayout();
        }
    }

    /**
     * Draws loots for each car, level, type.
     */
    public void drawLoots(List<Loot> loots) {
        if (loots.isEmpty()) {
            invalidate();
        } else {
            LootUtility holder = new LootUtility(loots, nrOfCars);
            holder.divideInCarLevelType();

            int purse;
            int jewel;
            int strongbox;

            for (int car = 0; car < nrOfCars; car++) {
                // BOTTOM
                purse = holder.getPurses(car, Level.BOTTOM);
                jewel = holder.getJewels(car, Level.BOTTOM);
                strongbox = holder.getStrongboxes(car, Level.BOTTOM);
                lootLayouts.get(car).get(Level.BOTTOM).setLoots(purse, jewel, strongbox);

                // TOP
                purse = holder.getPurses(car, Level.TOP);
                jewel = holder.getJewels(car, Level.TOP);
                strongbox = holder.getStrongboxes(car, Level.TOP);
                lootLayouts.get(car).get(Level.TOP).setLoots(purse, jewel, strongbox);
            }
        }
    }
}
