package ch.uzh.ifi.seal.soprafs16.coltexpress.service;

import android.content.Context;

import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * The {@code GameJoinService} sends request to join and choose a character a game
 * Created by a-a-hofmann on 13/04/16.
 */
public class GameJoinService {

    private static Logger logger = Logger.getLogger(GameJoinService.class.getName());

    /**
     * Context for rest service.
     */
    private Context context;

    /**
     * Player service.
     */
    private PlayerService playerService;

    /**
     * Singleton instance.
     */
    private static GameJoinService gameJoinService;

    /**
     * Private constructor to avoid instantiation.
     */
    private GameJoinService() {
    }

    /**
     * Singleton method.
     *
     * @param context Context for rest service.
     * @return an instance of this class.
     */
    public static GameJoinService getInstance(Context context) {
        if (gameJoinService == null) {
            gameJoinService = new GameJoinService();
        }
        gameJoinService.context = context;
        gameJoinService.playerService = PlayerService.getInstance(context);

        return gameJoinService;
    }

    /**
     * Joins player to a given game.
     *
     * @param gameId    Id of the game to join.
     * @param player    Player that wants to join a game.
     * @param character Character chosen from the player.
     */
    public void joinGame(final Long gameId, Player player, Character character) {
        logger.info("Player " + player.getUsername() + " is joining game " + gameId);

        RestService.getGamesApiInstance(context).joinGame(gameId, player.getToken(), character, new Callback<Player>() {
            @Override
            public void success(Player playerResponse, Response response) {
                playerService.setPlayer(playerResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                logError(error);
            }
        });
    }

    /**
     * If a player was already added to a game, selects an available characters.
     *
     * @param gameId    Id of the game that we want to join with given character.
     * @param player    Player that chose.
     * @param character Character chosen from the player.
     */
    public void chooseCharacter(final Long gameId, final Player player, final Character character) {
        logger.info("Player " + player.getUsername() + " is joining game " + gameId + " as owner");

        RestService.getGamesApiInstance(context).joinGameAsOwner(gameId, player.getToken(), character, new Callback<Player>() {
            @Override
            public void success(Player playerResponse, Response response) {
                playerService.setPlayer(playerResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                logError(error);
            }
        });
    }

    /**
     * Logs retrofit errors
     *
     * @param error Error object.
     */
    private void logError(RetrofitError error) {
        logger.severe(error.getUrl());
        logger.severe(error.getMessage());
    }
}