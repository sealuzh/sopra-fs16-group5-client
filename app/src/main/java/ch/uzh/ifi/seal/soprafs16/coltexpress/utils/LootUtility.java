package ch.uzh.ifi.seal.soprafs16.coltexpress.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Level;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Loot;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.LootType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;

/**
 * The class {@code LootUtility} handles the counting of loots by various parameters.
 * Created by a-a-hofmann on 19/04/16.
 */
public class LootUtility {

    private final int numberOfCars;

    private List<Loot> loots;

    private List<Map<Level, Map<LootType, Integer>>> lootsCarLevelType;

    public LootUtility(List<Loot> loots, final int numberOfCars) {
        this.loots = loots;
        this.numberOfCars = numberOfCars;
        lootsCarLevelType = new ArrayList<>();
    }

    public void setLoots(List<Loot> loots) {
        this.loots = loots;

        // Updates loots mapping.
        divideInCarLevelType();
    }

    /**
     * Gets purses for a given car and level
     *
     * @param car   Car to get loots for
     * @param level Level to get loots for
     * @return Loots in car level.
     */
    public Integer getPurses(int car, Level level) {
        return lootsCarLevelType.get(car).get(level).get(LootType.PURSE);
    }

    /**
     * Gets jewels for a given car and level
     *
     * @param car   Car to get loots for
     * @param level Level to get loots for
     * @return Loots in car level.
     */
    public Integer getJewels(int car, Level level) {
        return lootsCarLevelType.get(car).get(level).get(LootType.JEWEL);
    }

    /**
     * Gets strongboxes for a given car and level
     *
     * @param car   Car to get loots for
     * @param level Level to get loots for
     * @return Loots in car level.
     */
    public Integer getStrongboxes(int car, Level level) {
        return lootsCarLevelType.get(car).get(level).get(LootType.STRONGBOX);
    }

    /**
     * Facade for the lootsCarLevelType map.
     *
     * @param car   Car index.
     * @param level
     * @return
     */
    public Map<LootType, Integer> getByCarLevel(int car, Level level) {
        return lootsCarLevelType.get(car).get(level);
    }

    /**
     * Divides loots based on car, level and type. Basically the entry point.
     * Generates a List where at each position there is a Map representing the loot state of a car.
     * The map contains the loots by level and then by type.
     * Use getters to more easily get list of loots.
     */
    public void divideInCarLevelType() {
        // Empty list in case of an update and count again
        lootsCarLevelType.clear();

        // Sort by car
        Collections.sort(loots, new Comparator<Loot>() {
            @Override
            public int compare(Loot lhs, Loot rhs) {
                if (lhs.getCar() < rhs.getCar()) {
                    return -1;
                } else if (lhs.getCar() > rhs.getCar()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        // For each car getInstance a mapping between level and type
        for (int i = 0; i < numberOfCars; i++) {
            lootsCarLevelType.add(createCarMap(i));
        }
    }

    /**
     * Creates the mapping for a car from its level to the type and numberOfCars of loots per type.
     *
     * @param car Position starting from loc (0).
     * @return Map representing the loot state in this car.
     */
    private Map<Level, Map<LootType, Integer>> createCarMap(Integer car) {
        // Create car map
        Map<Level, Map<LootType, Integer>> carMap = new HashMap<>();

        // Divide loots by car
        List<Loot> lootsInCurrentCar = lootsByCar(car);

        // Divide loots in car by level
        List<Loot> lootsBottom = lootsPerLevel(lootsInCurrentCar, Level.BOTTOM);
        List<Loot> lootsTop = lootsPerLevel(lootsInCurrentCar, Level.TOP);

        // For each level put numberOfCars of loots per type in map
        carMap.put(Level.BOTTOM, countPerType(lootsBottom));
        carMap.put(Level.TOP, countPerType(lootsTop));

        return carMap;
    }

    /**
     * Gets list of loots by car.
     *
     * @param car The position from the loc (0)
     * @return Loots in car {@code car}
     */
    private List<Loot> lootsByCar(Integer car) {
        List<Loot> result = new ArrayList<>();
        for (Loot l : loots) {
            if (l.getCar().equals(car)) {
                result.add(l);
            }
        }

        return result;
    }

    /**
     * Divides loots by level.
     *
     * @param level The level to filter by
     * @return Loots in level {@code level}
     */
    private List<Loot> lootsPerLevel(Level level) {
        return lootsPerLevel(loots, level);
    }

    /**
     * Given a list of loots it divides it by level.
     *
     * @param lootsInCar The loots in this car
     * @param level      The level to filter by.
     * @return
     */
    private List<Loot> lootsPerLevel(List<Loot> lootsInCar, Level level) {
        List<Loot> result = new ArrayList<>();

        for (Loot l : lootsInCar) {
            if (level.equals(l.getLevel())) {
                result.add(l);
            }
        }

        return result;
    }

    /**
     * Prepares a map between LootType and Integer.
     *
     * @return map.
     */
    private static Map<LootType, Integer> initTypeCounter() {
        Map<LootType, Integer> typeCounter = new HashMap<>();
        typeCounter.put(LootType.PURSE, 0);
        typeCounter.put(LootType.JEWEL, 0);
        typeCounter.put(LootType.STRONGBOX, 0);

        return typeCounter;
    }

    /**
     * Counts how many loots by type there are and saves it in a map for ease of accessing.
     *
     * @return Map between LootType and how many loots there are.
     */
    private Map<LootType, Integer> countPerType() {
        return countPerType(loots);
    }

    /**
     * Counts how many loots by type there are and saves it in a map for ease of accessing.
     *
     * @param loots Loots to count per type.
     * @return Map between LootType and how many loots there are.
     */
    public static Map<LootType, Integer> countPerType(List<Loot> loots) {
        Map<LootType, Integer> typeCounter = initTypeCounter();

        int purse = 0;
        int jewel = 0;
        int strongbox = 0;

        for (Loot l : loots) {
            LootType type = l.getType();

            switch (type) {
                case PURSE_BIG:
                case PURSE_SMALL:
                case PURSE:
                    purse++;
                    break;
                case JEWEL:
                    jewel++;
                    break;
                case STRONGBOX:
                    strongbox++;
                    break;
                default:
                    break;
            }
        }

        typeCounter.put(LootType.PURSE, purse);
        typeCounter.put(LootType.JEWEL, jewel);
        typeCounter.put(LootType.STRONGBOX, strongbox);

        return typeCounter;
    }

    public static Map<LootType, Integer> getLootMapForPlayer(Player p) {
        return countPerType(p.getLoots());
    }

    public static Map<LootType, Integer> getLootValueMap(Player p) {

        Integer countPurse = 0;
        Integer countJewel = 0;
        Integer countStrongbox = 0;

        for (Loot l : p.getLoots()) {
            if (l != null) {
                switch (l.getType()) {
                    case PURSE_SMALL:
                    case PURSE_BIG:
                    case PURSE:
                        countPurse += l.getValue();
                        break;
                    case JEWEL:
                        countJewel += l.getValue();
                        break;
                    case STRONGBOX:
                        countStrongbox += l.getValue();
                        break;
                }
            }
        }

        Map<LootType, Integer> lootValues = new HashMap<>();
        lootValues.put(LootType.PURSE, countPurse);
        lootValues.put(LootType.JEWEL, countJewel);
        lootValues.put(LootType.STRONGBOX, countStrongbox);

        return lootValues;
    }

    public static List<Loot> filterLootsByType(List<Loot> loots, LootType lootType) {
        List<Loot> result = new ArrayList<>();

        // Get all loots of the type the player chose
        for (Loot loot : loots) {
            if (lootType.equals(loot.getType()) || (lootType.equals(LootType.PURSE) && isPurse(loot))) {
                result.add(loot);
            }
        }

        return result;
    }

    public static boolean isPurse(Loot loot) {
        return LootType.PURSE.equals(loot.getType()) ||
                LootType.PURSE_BIG.equals(loot.getType()) ||
                LootType.PURSE_SMALL.equals(loot.getType());
    }
}