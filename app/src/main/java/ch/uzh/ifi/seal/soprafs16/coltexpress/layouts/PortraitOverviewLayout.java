package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Map;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoaderFactory;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.LootType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.LootUtility;

/**
 * Created by alex on 05.05.2016.
 */
public class PortraitOverviewLayout extends LinearLayout {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(PortraitOverviewLayout.class.getSimpleName());

    private ImageView portrait;

    private TextView username;

    private ImageView bullets;

    private TextView bags;

    private TextView diamonds;

    private TextView suitcases;

    private ColorMatrixColorFilter bwFilter;

    private Player target;

    public PortraitOverviewLayout(Context context) {
        super(context);
        initializeViews(context, null);
    }

    public PortraitOverviewLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context, attrs);
    }

    private void initializeViews(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.portrait_and_info_layout, this, true);

        // get all attributes
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.PortraitOverviewLayout);
        Drawable portraitDrawable = ta.getDrawable(R.styleable.PortraitOverviewLayout_portrait);
        ta.recycle();

        portrait = (ImageView) findViewById(R.id.portrait);
        // if portrait image hasn't been set in xml, use tuco as default
        if (portraitDrawable == null) {
            portrait.setImageDrawable(getResources().getDrawable(R.drawable.tuco));
        } else {
            portrait.setImageDrawable(portraitDrawable);
        }

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        bwFilter = new ColorMatrixColorFilter(matrix);

        username = (TextView) findViewById(R.id.myUsername);
        bullets = (ImageView) findViewById(R.id.myBulletsPic);
        bags = (TextView) findViewById(R.id.my_bag);
        diamonds = (TextView) findViewById(R.id.my_diamond);
        suitcases = (TextView) findViewById(R.id.my_suitcase);
    }

    public void setInfo(Player player) {
        // image
        portrait.setImageDrawable(ImageLoaderFactory.createLoader(getContext(), player.getCharacter()).getHalfPortrait());

        // username
        username.setText(player.getUsername());

        // bullets
        bullets.setImageDrawable(ImageLoaderFactory.getBulletPic(getContext(), player.getBullets()));

        // loots
        Map<LootType, Integer> loots = LootUtility.getLootMapForPlayer(player);
        bags.setText(loots.get(LootType.PURSE).toString());

        diamonds.setText(loots.get(LootType.JEWEL).toString());

        suitcases.setText(loots.get(LootType.STRONGBOX).toString());

        this.target = player;
    }

    public void setEnabled(boolean enabled) {
        if (!enabled) {
            portrait.setColorFilter(bwFilter);
        } else {
            portrait.clearColorFilter();
        }
    }

    public Player getTarget() {
        return target;
    }
}