package ch.uzh.ifi.seal.soprafs16.coltexpress.service;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 * Created by francesca on 29.04.16.
 */
public class ReplaceFont {

    private static final Logger logger = Logger.getLogger(ReplaceFont.class.getSimpleName());

    public static void replaceDefaultFont(Context context, String defaultFont, String newFont) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), newFont);
        replaceFont(defaultFont, customFont);
    }

    private static void replaceFont(String defaultFont, Typeface customFont) {

        try {

            Field myField = Typeface.class.getDeclaredField(defaultFont);
            myField.setAccessible(true);
            myField.set(null, customFont);

        } catch (NoSuchFieldException e) {
            logger.severe(e.getMessage());
        } catch (IllegalAccessException e) {
            logger.severe(e.getMessage());
        }

    }
}
