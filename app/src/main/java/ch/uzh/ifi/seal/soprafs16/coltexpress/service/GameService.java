package ch.uzh.ifi.seal.soprafs16.coltexpress.service;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.GameDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Game;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.Player;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * The {@code GameService} class handles getting a single game and getting the players for a game.
 * Created by a-a-hofmann on 13/04/16.
 */
public class GameService {

    public interface GameCreatedListener {
        void onGameCreated();
    }

    public interface GameListReceivedListener {
        void onGameListReceived(List<Game> games);
    }

    public interface GameReceivedListener {
        void onGameReceived(Game result);
    }

    public interface GameStartListener {
        void onGameStarted();
    }

    private static Logger logger = Logger.getLogger(GameService.class.getName());

    /**
     * Cached version of last game that was retrieved.
     */
    private Game game;

    /**
     * Context. Needed for retrofit.
     */
    private Context context;

    /**
     * List of games for game menu.
     */
    private List<Game> games;

    /*
    Listeners for various events
     */
    private GameReceivedListener gameReceivedListener;
    private GameListReceivedListener gameListReceivedListener;
    private GameStartListener gameStartListener;
    private GameCreatedListener gameCreatedListener;

    /**
     * Singleton instance.
     */
    private static GameService gameService;

    /**
     * Private constructor to avoid instantiation.
     */
    private GameService() {
        games = new ArrayList<>();
    }

    /**
     * Get instance of singleton class.
     *
     * @return Instance of this class.
     */
    public static GameService getInstance(Context context) {
        if (gameService == null) {
            gameService = new GameService();
        }
        gameService.context = context;
        return gameService;
    }

    /**
     * Gets cached game.
     *
     * @return Game.
     */
    public Game getGame() {
        return game;
    }

    /**
     * Sets the cached game.
     *
     * @param game game to be cached.
     */
    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * Sends a GET to /games.
     *
     * @param gameId Id of requested game.
     */
    public void getGameFromNetwork(final Long gameId) {
        RestService.getGamesApiInstance(context).getGame(gameId, new Callback<Game>() {
            @Override
            public void success(Game gameResponse, Response response) {
                game = gameResponse;
                if (gameReceivedListener != null) {
                    gameReceivedListener.onGameReceived(game);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logError(error);
            }
        });
    }

    /**
     * Gets a list of games.
     */
    public void getGames() {
        RestService.getGamesApiInstance(context).getGames(new Callback<List<Game>>() {
            @Override
            public void success(List<Game> gameList, Response response) {
                games = gameList;
                if (gameListReceivedListener != null) {
                    gameListReceivedListener.onGameListReceived(games);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logError(error);
            }
        });
    }

    /**
     * Creates a game.
     *
     * @param gameRequest Game object containing the game name.
     * @param player      Owner of this game.
     */
    public void createGame(GameDTO gameRequest, Player player) {
        RestService.getGamesApiInstance(context).createGame(gameRequest, player.getToken(), new Callback<Game>() {
            @Override
            public void success(Game gameResponse, Response response) {
                game = gameResponse;
                if (gameCreatedListener != null) {
                    gameCreatedListener.onGameCreated();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logError(error);
            }
        });
    }

    /**
     * Starts a game.
     *
     * @param gameId Id of game to start.
     * @param token  Token of game owner.
     */
    public void startGame(final Long gameId, final String token) {
        RestService.getGamesApiInstance(context).startGame(gameId, token, new Callback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
                fireGameStarted();
            }

            @Override
            public void failure(RetrofitError error) {
                logError(error);
            }
        });
    }

    /**
     * Starts a quick game.
     *
     * @param gameId Id of game to start.
     * @param token  Token of game owner.
     */
    public void startQuickGame(final Long gameId, final String token) {
        RestService.getGamesApiInstance(context).startQuickGame(gameId, token, new Callback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
                fireGameStarted();
            }

            @Override
            public void failure(RetrofitError error) {
                logError(error);
            }
        });
    }

    /**
     * Exits the current game by sending an exit signal.
     *
     * @param gameId Id of the game to quit.
     * @param token  Token of the player that is quitting.
     */
    public void exitGame(final Long gameId, final String token) {
        RestService.getGamesApiInstance(context).exitGame(gameId, token, new Callback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    /**
     * Logs retrofit errors
     *
     * @param error Error object.
     */
    private void logError(RetrofitError error) {
        logger.severe(error.getUrl());
        logger.severe(error.getMessage());
    }

    /**
     * Sets the listener for game received event.
     *
     * @param observer Observer waiting for an update.
     */
    public void setOnGameReceivedListener(GameReceivedListener observer) {
        logger.severe("Registering new listener");
        gameReceivedListener = observer;
    }

    /**
     * Removes game received listener.
     */
    public void removeOnGameReceivedListener() {
        gameReceivedListener = null;
    }

    /**
     * Sets the listener for game list received event.
     *
     * @param listener Observer waiting for an update.
     */
    public void setOnGameListReceivedListener(GameListReceivedListener listener) {
        gameListReceivedListener = listener;
    }

    /**
     * Removes game list listener.
     */
    public void removeOnGameListReceivedListener() {
        gameListReceivedListener = null;
    }

    /**
     * Sets the listener for game creation event.
     *
     * @param listener Observer waiting for an update.
     */
    public void setOnGameCreatedListener(GameCreatedListener listener) {
        gameCreatedListener = listener;
    }

    /**
     * Removes game created listener.
     */
    public void removeOnGameCreatedListener() {
        gameCreatedListener = null;
    }

    /**
     * Sets the listener for game start event.
     *
     * @param listener Observer waiting for an update.
     */
    public void setOnGameStartedListener(GameStartListener listener) {
        gameStartListener = listener;
    }

    /**
     * Removes game started.
     */
    public void removeOnGameStartedListener() {
        gameStartListener = null;
    }

    /**
     * Notifies listener that the game started.
     */
    public void fireGameStarted() {
        if (gameStartListener != null) {
            gameStartListener.onGameStarted();
        }
    }
}