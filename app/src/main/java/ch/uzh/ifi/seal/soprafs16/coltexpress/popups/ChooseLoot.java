package ch.uzh.ifi.seal.soprafs16.coltexpress.popups;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.dtos.PossibilityDTO;
import ch.uzh.ifi.seal.soprafs16.coltexpress.layouts.LootPopupLayout;
import ch.uzh.ifi.seal.soprafs16.coltexpress.models.LootType;
import ch.uzh.ifi.seal.soprafs16.coltexpress.utils.LootUtility;

/**
 * Handles the choose target overview.
 * Created by alex on 05.05.2016.
 */
public class ChooseLoot implements IChoosePopup {

    public interface OnChoiceListener extends ChoiceListener {
        void onLootChosen(LootType lootType);
    }

    private static final Logger logger = Logger.getLogger(ChooseLoot.class.getSimpleName());

    private View anchor;

    private FrameLayout parent;

    private Context context;

    private PopupWindow popupWindow;

    private LinearLayout container;

    private PossibilityDTO possibilities;

    private OnChoiceListener listener;

    private View lootView;

    private LootType chosenLoot;

    public ChooseLoot(View anchor, FrameLayout parent, Context context) {
        this.anchor = anchor;
        this.parent = parent;
        this.context = context;

        init();
    }

    /**
     * Initialize Popup.
     */
    private void init() {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        lootView = layoutInflater.inflate(R.layout.loot_popup_layout, null);
        container = (LinearLayout) lootView.findViewById(R.id.parent);

        logger.severe("Opening popup!");

        popupWindow = new PopupWindow(
                lootView,
                500,
                218);
    }

    /**
     * Dismisses current Popup.
     */
    private void dismiss() {
        popupWindow.dismiss();
        parent.getForeground().setAlpha(0);
    }

    public void setPossibilities(PossibilityDTO possibilities) {
        this.possibilities = possibilities;
    }

    /**
     * Shows view to choose a player target.
     */
    public void show() {
        lootView.getBackground().setAlpha(150);
        popupWindow.showAsDropDown(anchor, -80, -100);

        // Drop existing opponents overview
        container.removeAllViews();

        final LootPopupLayout l = new LootPopupLayout(lootView.getContext());

        l.getChooseButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chosenLoot = l.getChosenType();

                if (listener != null && chosenLoot != null) {

                    listener.onLootChosen(chosenLoot);

                    dismiss();
                }
            }
        });

        // Set player info
        l.setLootNumber(LootUtility.countPerType(possibilities.getLoots()));

        container.addView(l);
    }

    @Override
    public void addOnChoiceListener(ChoiceListener listener) {
        this.listener = (OnChoiceListener) listener;
    }
}