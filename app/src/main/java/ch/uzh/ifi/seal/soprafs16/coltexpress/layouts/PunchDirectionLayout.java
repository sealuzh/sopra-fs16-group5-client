package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.logging.Logger;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;

public class PunchDirectionLayout extends LinearLayout {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(PunchDirectionLayout.class.getSimpleName());

    private Button chooseButton;

    private boolean isRight;

    private ColorMatrixColorFilter bwFilter;

    public PunchDirectionLayout(Context context, boolean isRightActive, boolean isLeftActive) {
        super(context);
        initializeViews(context, isRightActive, isLeftActive);
    }

    private void initializeViews(Context context, boolean isRightActive, boolean isLeftActive) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.punch_direction_popup, this);

        chooseButton = (Button) findViewById(R.id.choose);
        chooseButton.setEnabled(false);
        chooseButton.setVisibility(INVISIBLE);

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        bwFilter = new ColorMatrixColorFilter(matrix);

        final ImageView rightImage = (ImageView) findViewById(R.id.right);
        final ImageView leftImage = (ImageView) findViewById(R.id.left);

        if (isRightActive) {
            rightImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    isRight = true;

                    rightImage.clearColorFilter();
                    leftImage.setColorFilter(bwFilter);

                    chooseButton.setVisibility(VISIBLE);
                    chooseButton.setEnabled(true);
                }
            });
        } else {
            rightImage.setVisibility(INVISIBLE);
        }

        if (isLeftActive) {
            leftImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    isRight = false;

                    leftImage.clearColorFilter();
                    rightImage.setColorFilter(bwFilter);

                    chooseButton.setVisibility(VISIBLE);
                    chooseButton.setEnabled(true);
                }
            });
        } else {
            leftImage.setVisibility(INVISIBLE);
        }
    }

    public boolean isRight() {
        return isRight;
    }

    public Button getChooseButton() {
        return chooseButton;
    }
}
