package ch.uzh.ifi.seal.soprafs16.coltexpress.layouts;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;
import ch.uzh.ifi.seal.soprafs16.coltexpress.constants.Character;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoader;
import ch.uzh.ifi.seal.soprafs16.coltexpress.loaders.ImageLoaderFactory;
import it.sephiroth.android.library.tooltip.Tooltip;


/**
 * Created by francesca on 21.04.16.
 */
public class OtherPlayerOverviewLayout extends LinearLayout {

    private ImageView portrait;
    private TextView textUsername;
    private ImageView ability;
    private TextView textBag;
    private TextView textDiamond;
    private TextView textSuitcase;
    private ImageView bullet;
    private Map<Character, String> abilityDescriptionMap;


    public OtherPlayerOverviewLayout(Context context) {
        super(context);
        initializeViews(context);
    }

    public OtherPlayerOverviewLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public OtherPlayerOverviewLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }


    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.other_player_overview_layout, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        portrait = (ImageView) findViewById(R.id.charac);
        textUsername = (TextView) findViewById(R.id.user);
        ability = (ImageView) findViewById(R.id.abil);
        textBag = (TextView) findViewById(R.id.bagText);
        textDiamond = (TextView) findViewById(R.id.diamondText);
        textSuitcase = (TextView) findViewById(R.id.suitcaseText);
        bullet = (ImageView) findViewById(R.id.bullets);
    }

    public void setOpponent(final Character character, String username, Integer purses,
                            Integer jewels, Integer suitcases, Integer bullets) {

        ImageLoader loader = ImageLoaderFactory.createLoader(getContext(), character);

        portrait.setImageDrawable(loader.getHalfPortrait());
        ability.setImageDrawable(loader.getSpecialAbility());
        ability.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showAbilDescription(character);
            }
        });

        abilityDescriptionMap = new HashMap<>();
        abilityDescriptionMap.put(Character.BELLE,
                getContext().getString(R.string.belle_ability_desc));

        abilityDescriptionMap.put(Character.CHEYENNE,
                getContext().getString(R.string.cheyenne_ability_desc));

        abilityDescriptionMap.put(Character.DJANGO,
                getContext().getString(R.string.django_ability_desc));

        abilityDescriptionMap.put(Character.TUCO,
                getContext().getString(R.string.tuco_ability_desc));

        abilityDescriptionMap.put(Character.DOC,
                getContext().getString(R.string.doc_ability_desc));

        abilityDescriptionMap.put(Character.GHOST,
                getContext().getString(R.string.ghost_ability_desc));

        textUsername.setText(username);

        // Set player loots
        textBag.setText(purses.toString());
        textDiamond.setText(jewels.toString());
        textSuitcase.setText(suitcases.toString());

        // bullets correspond to the number of bullets a player could still shoot
        if (bullets == 0) {
            bullet.setVisibility(INVISIBLE);
        } else {
            bullet.setImageDrawable(ImageLoaderFactory.getBulletPic(getContext(), bullets));
        }
    }

    public void setHighlighted(boolean isHighlighted) {
        Drawable border = getBackground();
        if (isHighlighted) {
            border.setAlpha(255);
        } else {
            border.setAlpha(0);
        }
    }

    public void showAbilDescription(Character character) {
        Tooltip.make(getContext(),
                new Tooltip.Builder(101)
                        .anchor(ability, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 10000)
                        .activateDelay(800)
                        .showDelay(300)
                        .text(abilityDescriptionMap.get(character))
                        .maxWidth(500)
                        .withArrow(true)
                        .withOverlay(false)
                        .withStyleId(R.style.ToolTipLayoutCustomStyle)
                        .floatingAnimation(Tooltip.AnimationBuilder.SLOW)
                        .build()
        ).show();
    }
}