package ch.uzh.ifi.seal.soprafs16.coltexpress.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import ch.uzh.ifi.seal.soprafs16.coltexpress.R;

public class GameExitFragment extends DialogFragment {

    public interface OnGameExitConfirmedListener {
        void onGameExitConfirmed();
    }

    private OnGameExitConfirmedListener onGameExitConfirmedListener;

    public void setOnGameExitConfirmedListener(OnGameExitConfirmedListener onGameExitConfirmedListener) {
        this.onGameExitConfirmedListener = onGameExitConfirmedListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.game_exit);

        builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (onGameExitConfirmedListener != null) {
                    onGameExitConfirmedListener.onGameExitConfirmed();
                }
            }
        });
        builder.setNegativeButton(R.string.dismiss, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dismiss();
            }
        });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
