package ch.uzh.ifi.seal.soprafs16.coltexpress.constants;

/**
 * Created by a-a-hofmann on 14/04/16.
 */
public enum GameStatus {
    PENDING, PLANNINGPHASE, ACTIONPHASE, FINISHED
}
